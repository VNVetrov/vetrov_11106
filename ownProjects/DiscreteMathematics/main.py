import itertools


def main(f,number_of_variables, array_of_values):
    array_of_combinations = get_data(number_of_variables, array_of_values)
    SDNF = getting_SDNF(array_of_combinations)
    SDNF_changeable = SDNF[:]
    abbreviated_dnf = getting_abbreviated_dnf(SDNF_changeable, number_of_variables)
    array_with_true_combination = getting_only_true_comb(array_of_combinations)
    array_of_conjs_of_disjs = transformed_kwain_table(abbreviated_dnf,array_with_true_combination,number_of_variables)
    array_of_conjs_of_disjs.sort(key=len)
    array_of_final_disjs = getting_final_array(array_of_conjs_of_disjs)
    array_of_final_dnfs = getting_array_of_final_dnfs(array_of_final_disjs)



    print_table(f,number_of_variables, array_of_values)

    f.write('Значения 1: ' + str(array_with_true_combination) + '\n')
    f.write('СДНФ: '+ str(SDNF)+'\n')
    f.write('Cокр ДНФ: ' + str(abbreviated_dnf)+'\n')
    f.write('Преобр табл Квайна: ' + str(array_of_conjs_of_disjs)+'\n')
    f.write('Финальная кон-ия диз-тов: ' +str(array_of_final_disjs) + '\n' )
    f.write('\n\n')


    print("Значения 1: ",array_with_true_combination)
    print("СДНФ: ", SDNF)
    print("Cокр ДНФ: ", abbreviated_dnf)
    print("Преобр таб Квайна: ",array_of_conjs_of_disjs)
    print('Финальная кон-ия диз-тов: ',array_of_final_disjs)
    print("Тупиковые днф: ")
    for list in array_of_final_dnfs:
        print(list)
    print("\n\n")


def print_table(f,n, array):
    # printing table (a b c...x y   F)
    letters = "abcdefghiklmnopqrst"
    for i in range(n):
        f.write(letters[i] + ' ')
        print(letters[i], sep="", end=" ")
    f.write(" F\n")
    print("  F")
    h = 0
    for d in itertools.product('01', repeat=n):
        for i in range(n):
            f.write(d[i] + ' ')
            print(d[i], sep="", end=" ")
        f.write(" " + str(array[h])+'\n')
        print(" ", array[h])
        h += 1


def get_data(n, array):
    # getting array_of_combinations from array_of_values array
    array_of_combinations = []
    h = 0
    for d in itertools.product('01', repeat=n):
        combination = separate(d)
        actual_dictionary = {array[h]: combination}
        array_of_combinations.append(actual_dictionary)
        h += 1
    return array_of_combinations

def getting_only_true_comb(array_of_combinations):
    array_with_true_comb = []
    for dict in array_of_combinations:
        if dict.keys() == {1}:
            array_with_true_comb.append(dict[1])
    return  array_with_true_comb


def separate(d):
    # we got "1010" - we get [1,0,1,0]
    combination = []
    for x in d:
        combination.append(int(x))
    return combination


# -1 - not(x); 0 - nothing; 1 - x;
def getting_SDNF(array):
    # getting SDNF from array_of_values array
    SDNF = []
    for i in range(len(array)):
        conjunct = []
        if array[i].keys() == {1}:
            for x in array[i][1]:
                if x == 0:
                    conjunct.append(-1)
                else:
                    conjunct.append(1)
            SDNF.append(conjunct)
    return SDNF


def getting_abbreviated_dnf(SDNF_changeable, number_of_variables):
    # getting abbreviated DNF by reducing extra conjuncts
    abbreviated_dnf = []
    for c in range(number_of_variables - 1):
        conjuncts = getting_conjuncts(SDNF_changeable, number_of_variables)
        if len(conjuncts) == 0:
            return SDNF_changeable
        array_of_del_index = delete(conjuncts, SDNF_changeable, number_of_variables)
        for i in range(len(array_of_del_index) - 1, -1, -1):
            del SDNF_changeable[array_of_del_index[i]]
        for conjunct in conjuncts:
            if conjunct not in SDNF_changeable:
                SDNF_changeable.append(conjunct)
    abbreviated_dnf = SDNF_changeable
    return abbreviated_dnf


def getting_conjuncts(SDNF, number_of_variables):
    # getting new conjuncts with (N-1) lenght
    conjuncts = []
    for i in range(len(SDNF)):
        for k in range(i + 1, len(SDNF)):
            difference_of_conjs = 0
            actual_conj = []
            for l in range(number_of_variables):
                if difference_of_conjs > 1:
                    break
                if SDNF[i][l] == SDNF[k][l]:
                    actual_conj.append(SDNF[i][l])
                else:
                    difference_of_conjs += 1
                    actual_conj.append(0)
            if difference_of_conjs == 1:
                conjuncts.append(actual_conj)
    return conjuncts


def delete(conjuncts, SDNF_changeable, number_of_variables):
    # getting index of elements of SDNF we have to delete
    array_of_del_index = []
    for i in range(len(SDNF_changeable)):
        for k in range(len(conjuncts)):
            flag = True
            for l in range(number_of_variables):
                if not (conjuncts[k][l] == 0 or conjuncts[k][l] == SDNF_changeable[i][l]):
                    flag = False
                    break
            if i not in array_of_del_index and flag:
                array_of_del_index.append(i)
    return array_of_del_index

def transformed_kwain_table(abbreviated_dnf,array_with_true_combination,number_of_variables):
    # getting array of conjunction of disjuncts
    array_of_conjs_of_disjs =[]
    for combination in array_with_true_combination:
        actual_disjunct = []
        for conjunct in abbreviated_dnf:
            if(is_true_on_comb(conjunct,combination)):
                actual_disjunct.append(conjunct)
        if len(actual_disjunct)!=0:
            array_of_conjs_of_disjs.append(actual_disjunct)
    return array_of_conjs_of_disjs

def is_true_on_comb(conjunct,combination):
    # checking is conjucnt==true on combination
    flag=True
    for i in range(len(conjunct)):
        if conjunct[i]==1 and combination[i]==1:
            continue
        elif conjunct[i]==-1 and combination[i]==0:
            continue
        elif conjunct[i]==0:
            continue
        else:
            flag=False
            break
    return flag

def getting_final_array(array_of_conjs_of_disjs):
    # get final array before multiplying disjuncts
    array_of_conjs_of_disjs_copy = array_of_conjs_of_disjs[:]
    for disjunct in array_of_conjs_of_disjs_copy:
            array_of_conjs_of_disjs_copy = delete_extra_disjs(disjunct,array_of_conjs_of_disjs_copy)
    return array_of_conjs_of_disjs_copy

def delete_extra_disjs(disjunct,array):
    # deleting extra disjuncts
    for another_disjunct in array:
        if disjunct!=another_disjunct:
            if dis_in_another_dis(disjunct,another_disjunct):
                del array[array.index(another_disjunct)]
    return array

def dis_in_another_dis(disjunct,anoter_disjunct):
    # cheking is one disjunct inside another disjunct
    disjunct_copy = disjunct[:]
    for i in range(len(anoter_disjunct)):
        if anoter_disjunct[i] in disjunct_copy:
            del disjunct_copy[disjunct_copy.index(anoter_disjunct[i])]
    if len(disjunct_copy)==0:
        return True
    else:
        return False

'''def getting_array_of_final_dnfs(array_of_final_disjs): # wrong
    array_of_final_dnfs = array_of_final_disjs[:]
    new_array = []
    counter=get_counter(array_of_final_dnfs)
    for c in range(counter):

        new_disjuncts = []

        if c+1>=len(array_of_final_dnfs):
            break
        for k in range(len(array_of_final_dnfs[c])):
            for l in range(len(array_of_final_dnfs[c+1])):
                conjunct = []
                for item in array_of_final_dnfs[c][k]:
                    conjunct.append(item)
                for item in array_of_final_dnfs[c+1][l]:
                    if item not in conjunct:
                        conjunct.append(item)
                if conjunct not in new_disjuncts:
                    new_disjuncts.append(conjunct)
        new_array.append(new_disjuncts)
        # del array_of_final_dnfs[1]
        # del array_of_final_dnfs[0]

    return  new_array'''

def getting_array_of_final_dnfs(array_of_final_disjs): # WRONG
    new_array = array_of_final_disjs[:]
    length = len(array_of_final_disjs)
    for disjunct in new_array:
        for another_disjunct in new_array:
            if disjunct==another_disjunct:
                continue
            else:
                conjunct = []
                for item in disjunct:
                    conjunct.append(item)
                    break
                for item in another_disjunct:
                    if item not in conjunct:
                        conjunct.append(item)
                        break
            if conjunct not in new_array:
                del new_array[0]
                new_array=[conjunct]+new_array
                # del new_array[1]
    return  new_array

def get_counter(array):
    lengths = []
    for item in array:
        lengths.append(len(item))
    multiply=1
    for x in lengths:
        multiply*=x
    return multiply

def same(array):
    #if f(1,1...1,1) or f(0,0..0,0) - no need to check
    flag = True
    first = array[0]
    for x in array:
        if x != first:
            flag = False
            break
    return flag


if __name__ == "__main__":
    f = open("output.txt", "w")
    for i in range(2, 4):
        number_of_variables = i
        for variant in itertools.product('01', repeat=2 ** number_of_variables):
            array_of_values = separate(variant)
            if not same(array_of_values):
                main(f,number_of_variables, array_of_values)
    f.close()