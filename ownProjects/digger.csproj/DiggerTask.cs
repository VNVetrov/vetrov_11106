﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Digger
{
    public class Player : ICreature
    {
        public CreatureCommand Act(int x, int y)
        {
            var digger = new CreatureCommand { };
            switch (Game.KeyPressed)
            {
                case System.Windows.Forms.Keys.Right:
                    if (x + 1 < Game.MapWidth) digger.DeltaX = 1;
                    break;
                case System.Windows.Forms.Keys.Left:
                    if (x - 1 >= 0) digger.DeltaX = -1;
                    break;
                case System.Windows.Forms.Keys.Up:
                    if (y - 1 >= 0) digger.DeltaY = -1;
                    break;
                case System.Windows.Forms.Keys.Down:
                    if (y + 1 < Game.MapHeight) digger.DeltaY = 1;
                    break;
            }
            return digger;
        }

        public bool DeadInConflict(ICreature conflictedObject)
        {
            return false;
        }

        public int GetDrawingPriority()
        {
            return 1;
        }

        public string GetImageFileName()
        {
            return "Digger.png";
        }
    }
    public class Terrain : ICreature
    {
        public CreatureCommand Act(int x, int y)
        {
            return new CreatureCommand();
        }

        public bool DeadInConflict(ICreature conflictedObject)
        {
            return true;
        }

        public int GetDrawingPriority()
        {
            return 1;
        }

        public string GetImageFileName()
        {
            return "Terrain.png";
        }
    }
}
