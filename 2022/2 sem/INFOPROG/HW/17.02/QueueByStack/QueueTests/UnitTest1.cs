using NUnit.Framework;
using System;
using System.Text;

namespace QueueTests
{
    public class Tests
    {
        [TestCase(new int[] { 1, 2, 3, 4 }, 1)]  
        [TestCase(new int[] { 5, 4, 3, 2 ,1 }, 5)]
        [TestCase(new int[] { 0 }, 0)]
        public void EnqueueAndDequeue(int[] args, int answer)
        {
            MyQueue<int> myQueue = new MyQueue<int>();
            for (int i = 0; i < args.Length; i++)
            {
                myQueue.Enqueue(args[i]);
            }
            Assert.AreEqual(answer, myQueue.Dequeue());
        }

        [TestCase(new int[] { 1, 2, 3 }, 2, new int[] { 1, 2 })]
        [TestCase(new int[] { 5, 4, 3, 2, 1 }, 4, new int[] { 5, 4, 3, 2 })]
        [TestCase(new int[] { 1 }, 1, new int[] { 1 })]
        public void EnqueueAndSomeDequeue(int[] args, int dequeueCount, int[] answer)
        {
            MyQueue<int> myQueue = new MyQueue<int>();
            var result = new int[dequeueCount];
            for (int i = 0; i < args.Length; i++)
            {
                myQueue.Enqueue(args[i]);
            }
            for (int i = 0;i < dequeueCount; i++)
            {
                result[i] = myQueue.Dequeue();
            }
            Assert.AreEqual(answer, result);
        }

        [Test]
        public void EmptyQueueTest()
        {
            Assert.Throws<Exception>(() => EnqueueAndSomeDequeue(new int[] { }, 1, new int[] { }));
        }
    }
}