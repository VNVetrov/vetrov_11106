﻿public class MyLinkedList<T>
{
    class MyLinkedListItem
    {
        public T Value { get; set; }
        public MyLinkedListItem Prev { get; set; }
    }

    public int Count { get; private set; }
    private MyLinkedListItem Last;

    public void Add(T item)
    {
        MyLinkedListItem node = new MyLinkedListItem { Value = item, Prev = Last };
        Last = node;
        Count++;
    }

    public T FindLast()
    {
        if (Count == 0) throw new Exception("Empty list");
        return Last.Value;
    }

    public void RemoveLast()
    {
        if (Count == 0) throw new Exception("Empty list");
        Last = Last.Prev;
        Count--;
    }
}


public class MyStack<T>
{
    private MyLinkedList<T> stackList = new MyLinkedList<T>();
    public int Count { get; private set; }

    public void Push(T item)
    {
        stackList.Add(item);
        Count++;
    }

    public T Pop()
    {
        if (stackList.Count == 0) throw new Exception("Empty stack");
        T result = stackList.FindLast();
        stackList.RemoveLast();
        Count--;
        return result;
    }
}

public class MyQueue<T>
{
    private MyStack<T> stackPush = new MyStack<T>();
    private MyStack<T> stackPop = new MyStack<T>();
    public void Enqueue(T item)
    {
        Transfer(stackPop, stackPush);
        stackPush.Push(item);
    }

    private void Transfer(MyStack<T> stackFrom, MyStack<T> stackTo)
    {
        int count = stackFrom.Count;
        for (int i = 0; i < count; i++)
        {
            stackTo.Push(stackFrom.Pop());
        }
    }

    public T Dequeue()
    {
        if (stackPop.Count == 0 && stackPush.Count == 0)
            throw new Exception("Empty queue");
        Transfer(stackPush, stackPop);
        return stackPop.Pop();
    }
}

public class Program
{
    public static void Main()
    {   
    }
}