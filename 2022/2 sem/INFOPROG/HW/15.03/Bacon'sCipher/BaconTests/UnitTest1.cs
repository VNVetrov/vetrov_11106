using NUnit.Framework;

namespace BaconTests
{
    public class Tests
    {
        [Test]
        public void BasicTest()
        {
            Assert.AreEqual(BaconsCipher.FindAllSubstrings("aaba"), 8);
            Assert.AreEqual(BaconsCipher.FindAllSubstrings("vova"), 9);
            Assert.AreEqual(BaconsCipher.FindAllSubstrings("abc"), 6);
            Assert.AreEqual(BaconsCipher.FindAllSubstrings("teeet"), 11);
        }
    }
}