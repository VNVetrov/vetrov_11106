﻿using System.Text;
using System;
using System.Collections.Generic;

public class Program
{
    public static void Main()
    {
    }
}

public class BaconsCipher
{
    public static int FindAllSubstrings(string text)
    {
        var sb = new StringBuilder();
        var hashes = new HashSet<string>();
        for (int length = 1; length <= text.Length; length++)
        {
            sb.Clear();
            string first = text.Substring(0, length);
            sb.Append(first);
            hashes.Add(first);
            for (int i = length; i < text.Length; i++)
            {
                sb.Append(text[i]);
                sb.Remove(0, 1);
                hashes.Add(sb.ToString());
            }
        }
        return hashes.Count;
    }
}