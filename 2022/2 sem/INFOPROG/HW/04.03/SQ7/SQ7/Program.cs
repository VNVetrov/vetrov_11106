﻿public class Program
{
    public static void Main()
    {
    }
}

public class VersionStack<T>
{
    class Node
    {
        public T? Value { get; set; }
        public Node? Prev { get; set; }
    }

    private Node? Last { get; set; }
    private List<Node> historyList = new List<Node>() { new Node() };

    public void Push(T item)
    {
        Node node = new Node() { Value = item};
        node.Prev = Last;
        Last = node;
        historyList.Add(node);
    }

    public T Pop()
    {
        if (Last == null) throw new Exception("Empty stack");
        T? result = Last.Value;
        Last = Last.Prev;
        historyList.Add(Last);
        return result;
    }

    public void Rollback(int status)
    {
        if (status <= historyList.Count)
            Last = historyList[status];
        else
            throw new Exception("Not able to rollback to this status");
    }

    public void Forget()
    {
        historyList.Clear();
        historyList.Add(new Node());
    }
}
