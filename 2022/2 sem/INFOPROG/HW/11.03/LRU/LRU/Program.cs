﻿using System;
using System.Collections.Generic;

namespace LRU
{
    class Program
    {
        static void Main()
        {
        }
    }

    class LRU<TValue>
    {
        class Node
        {
            public readonly int Key;
            public readonly TValue Value;
            public Node Prev { get; set; }
            public Node Next { get; set; }

            public Node(int key, TValue value)
            {
                Key = key;
                Value = value;
            }

            public Node Clear()
            {
                this.Next = this.Prev = null;
                return this;
            }
        }

        class MyLinkedList
        {
            private Node Last { get; set; }
            private Node First { get; set; }

            public void Add(Node node)
            {
                if(First == null)
                {
                    First = node;
                }
                else
                {
                    node.Prev = Last;
                    Last.Next = node;
                }
                Last = node;
            }

            public void RemoveExactNode(Node node)
            {
                if(node == First)
                {
                    RemoveFirst();
                }
                else if(node == Last)
                {
                    RemoveLast();
                }
                else
                {
                    node.Prev.Next = node.Next;
                    node.Next.Prev = node.Prev;
                }
                node.Clear();
            }

            public Node RemoveFirst()
            {
                if (First == null) throw new Exception("Empty list");
                Node node = First;
                First = First.Next;
                if (First != null)
                {
                    First.Prev = null;
                }
                else
                {
                    Last = null;
                }
                return node.Clear();
            }
            
            public Node RemoveLast()
            {
                if (First == null) throw new Exception("Empty list");
                Node node = Last;
                Last = Last.Prev;
                if (Last != null)
                {
                    Last.Next = null;
                }
                else
                {
                    First = null;
                }
                return node.Clear();
            }
        }

        Dictionary<int, Node> dictionary = new Dictionary<int, Node>();
        MyLinkedList list = new MyLinkedList();

        public void Add(int key, TValue value)
        {
            Node node = new Node(key, value);
            dictionary.Add(key, node);
            list.Add(node);
        }

        public TValue Get(int key)
        {
            if (!dictionary.ContainsKey(key)) throw new Exception("Key not found");
            Node node = dictionary[key];
            list.RemoveExactNode(node);
            list.Add(node);
            return node.Value;
        }

        public void RemoveLeastRecentlyUsed()
        {
            Node node = list.RemoveFirst();
            dictionary.Remove(node.Key);
            node.Clear();
        }
    }
}
