﻿public static class LinqExt
{
    public static IEnumerable<List<T>> GroupBy<T, TKey>(this IEnumerable<T> source, Func<T,TKey> selector)
    {
        Dictionary<TKey, List<T>> map = new Dictionary<TKey, List<T>>();
        foreach (var item in source)
        {
            if (!map.ContainsKey(selector(item)))
                map.Add(selector(item), new List<T>() { item });
            else
                map[selector(item)].Add(item);
        }
        foreach (var item in map.Values)
            yield return item;
    }
}

class Program
{
    public static void Main()
    {
    }
}