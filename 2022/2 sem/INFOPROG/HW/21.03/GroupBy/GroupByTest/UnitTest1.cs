using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace GroupByTest
{
    public class Tests
    {
        [Test]
        public void Test1()
        {
            Func<int, bool> selector = (x) => x % 2 == 0;
            var list = new List<int>() { 1, 2, 3, 4, 5, 6 };
            var result = list.GroupBy(selector);
            var sb = new StringBuilder();
            foreach (var item in result)
                foreach (var item2 in item)
                    sb.Append(item2);
            var answer = sb.ToString();
            Assert.AreEqual(answer, "135246");   
        }
    }
}