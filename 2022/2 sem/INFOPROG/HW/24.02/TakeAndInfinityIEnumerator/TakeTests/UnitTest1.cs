using NUnit.Framework;
using System.Collections.Generic;

namespace TakeTests
{
    public class Tests
    {
        [Test]
        public void TakeZeroOrLess()
        {
            var list = new List<int>() { 1, 2, 3, 4 };
            Assert.IsEmpty(list.Take(-1));
            Assert.IsEmpty(list.Take(0));
        }

        [Test]
        public void TakeLessThanCount()
        {
            var list = new List<int>() { 1, 2, 3, 4, 5 };
            Assert.AreEqual(new List<int> { 1, 2, 3 }, list.Take(3));
        }

        [Test]
        public void TakeMoreThanCount()
        {
            var list = new List<int>() { 1, 2, 3, 4, 5 };
            Assert.AreEqual(new List<int>() { 1, 2, 3, 4, 5 }, list.Take(5));
            Assert.AreEqual(new List<int>() { 1, 2, 3, 4, 5 }, list.Take(10));
        }
    }
}