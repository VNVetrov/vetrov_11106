﻿using System.Collections;
//Take Realisation
public static class IEnumerableExt
{
    public static IEnumerable<T?> Take<T>(this IEnumerable<T?> data, int number)
    {
        if (number <= 0)
        {
            yield break;
        }
        foreach (var item in data)
            if (number-- != 0)
                yield return item;
            else
                yield break;
    }
}
//InfinyNaturalSequence
public class InfinityNaturalSeq : IEnumerable<int>
{
    class InfinityNaturalSeqEnumerator : IEnumerator<int>
    {
        int N;
        public int Current
        {
            get { return N; }
        }

        object IEnumerator.Current { get { return Current; } }

        public bool MoveNext()
        {
            N++;
            return true;
        }
        #region
        public void Dispose()
        {
        }

        public void Reset()
        {
        }
        #endregion
    }
    public int N;

    public IEnumerator<int> GetEnumerator()
    {
        return new InfinityNaturalSeqEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}

public class Program
{
    public static void Main()
    {
    }
}