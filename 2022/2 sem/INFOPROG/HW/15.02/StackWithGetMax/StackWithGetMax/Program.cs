﻿public class MyLinkedList<T>
{
    class MyLinkedListItem
    {
        public T Value { get; set; }
        public MyLinkedListItem Prev { get; set; }
    }

    public int Count { get; private set; }
    private MyLinkedListItem Last;

    public void Add(T item)
    {
        MyLinkedListItem node = new MyLinkedListItem { Value = item, Prev = Last };
        Last = node;
        Count++;
    }

    public T FindLast()
    {
        if (Count == 0) throw new Exception("Empty list");
        return Last.Value;
    }

    public void RemoveLast()
    {
        if (Count == 0) throw new Exception("Empty list");
        Last = Last.Prev;
        Count--;
    }
}

public class MyStack<T> where T : IComparable
{
    private MyLinkedList<T> stackList = new MyLinkedList<T>();
    private MyLinkedList<T> maxs = new MyLinkedList<T>();

    public void Push(T item)
    {
        stackList.Add(item);
        if (maxs.Count == 0 || maxs.FindLast().CompareTo(item)<=0) 
            maxs.Add(item);
    }

    public T GetMax()
    {
        return maxs.Count != 0 ? maxs.FindLast() : throw new Exception("Empty stack");
    }

    public T Pop()
    {
        if (stackList.Count == 0) throw new Exception("Empty stack");
        T result = stackList.FindLast();
        if (result.CompareTo(maxs.FindLast())==0)
        {
            maxs.RemoveLast();
        }
        stackList.RemoveLast();
        return result;
    }
}

public class Program
{
    public static void Main()
    {

    }
}
