using NUnit.Framework;
using System;

namespace TestForStack
{
    public class Tests
    {
        [TestCase(new int[] {1},1)]
        [TestCase(new int[] { 1, 2, 3 }, 3)]
        [TestCase(new int[] { 1, 5, 3, 5, 4 }, 5)]
        [TestCase(new int[] { 7, 5, 3, 5, 4 }, 7)]
        public void JustFindMax(int[] args, int answer)
        {
            var stack = new MyStack<int>();
            foreach (var arg in args)
            {
                stack.Push(arg);
            }
            Assert.AreEqual(answer, stack.GetMax());
        }

        [TestCase(new int[] { 1, 5, 3, 5, 4 }, 2, 5)]
        [TestCase(new int[] { 1, 4, 3, 8, 4 }, 1, 8)]
        public void PopAndFindMax(int[] args, int popsAmount, int answer)
        {
            var stack = new MyStack<int>();
            foreach (var arg in args)
            {
                stack.Push(arg);
            }
            for (var i = 0; i < popsAmount; i++)
            {
                stack.Pop();
            }
            Assert.AreEqual(answer, stack.GetMax());
        }
        [Test]
        public void TestForExceptions()
        {
            Assert.Throws<Exception>(() => JustFindMax(new int[] { }, 0));
            Assert.Throws<Exception>(() => PopAndFindMax(new int[] { 1 }, 1, 0));
        }
    }
}