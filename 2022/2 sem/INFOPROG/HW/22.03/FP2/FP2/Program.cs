﻿static class LinqExt
{
    public static IEnumerable<List<string>> GroupBy(this IEnumerable<string> source, Func<string, int> selector)
    {
        Dictionary<int, List<string>> map = new Dictionary<int, List<string>>();
        foreach (var item in source)
        {
            if (!map.ContainsKey(selector(item)))
                map.Add(selector(item), new List<string>() { item });
            else
                map[selector(item)].Add(item);
        }
        foreach (var item in map.Values)
            yield return item;
    }
}

class Program 
{
    public static void Main()
    {
        string[] input = InputData();
        Dictionary<DayOfWeek, double> daysOfWeekDict = new Dictionary<DayOfWeek, double>();
        IEnumerable<List<string>> result = GroupDaysOfWeek(input);
        WorkWithData(daysOfWeekDict, result);
        OutputResult(daysOfWeekDict);
    }

    private static IEnumerable<List<string>> GroupDaysOfWeek(string[] input)
    {
        Func<string, int> selector = x => { return int.Parse(x.Split(' ')[0]) % 7; };
        var result = input.GroupBy(selector);
        return result;
    }

    private static void OutputResult(Dictionary<DayOfWeek, double> dict)
    {
        foreach (var date in dict)
            Console.WriteLine(date);
    }

    private static void WorkWithData(Dictionary<DayOfWeek, double> dict, IEnumerable<List<string>> result)
    {
        var currentDayOfWeek = DayOfWeek.Sunday;
        foreach (var day in result)
        {
            int counter = 0;
            foreach (var stats in day)
            {
                if (!dict.ContainsKey(currentDayOfWeek))
                    dict.Add(currentDayOfWeek, int.Parse(stats.Split(' ')[1]));
                else
                    dict[currentDayOfWeek] += int.Parse(stats.Split(' ')[1]);
                counter++;
            }
            dict[currentDayOfWeek] /= counter;
            currentDayOfWeek += 1;
        }
    }

    private static string[] InputData()
    {
        string path = @"YearLoad.txt";
        var input = File.ReadAllLines(path);
        return input;
    }
}