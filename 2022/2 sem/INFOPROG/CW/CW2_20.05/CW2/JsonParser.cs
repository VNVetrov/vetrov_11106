﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CW2
{
    public static class JsonParser
    {
        public static void Parse()
        {
            GetUrlsOfImages();
            Task task = DownloadImages(urlsOfImages);
            while(!task.IsCompleted)
                task.Wait();
        }
        public static List<string> urlsOfImages = new List<string>();
        public static async Task DownloadImages(List<string> urls)
        {
            var listOfImages = new List<byte[]>();
            var httpClient = new HttpClient();
            httpClient.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue("C#-program")));
            foreach (var url in urls)
            {
                var imageBytes = await httpClient.GetByteArrayAsync(url);
                listOfImages.Add(imageBytes);
                Console.WriteLine("{0}%",listOfImages.Count/5000 * 100);
            }
            foreach (var image in listOfImages)
            {
                string localPath = image.GetHashCode().ToString() + ".png";
                File.WriteAllBytes(localPath, image);
            }
            Console.WriteLine("Complete");
        }

        public static async void GetUrlsOfImages()
        {
            var rx = new Regex(@"http(s)?://([\w-]+\.)+[\w-]+(/[\w- ./?%&=]*)?"
                                ,RegexOptions.Compiled);
            var url = "https://jsonplaceholder.typicode.com/photos";
            using var client = new HttpClient();
            client.DefaultRequestHeaders.UserAgent.Add(new ProductInfoHeaderValue(new ProductHeaderValue("C#-program")));
            Task<string> task = client.GetStringAsync(url);
            Task.WaitAll(task);
            string jsonString = await task;
            var matches = rx.Matches(jsonString);
            foreach (Match match in matches)
                if (!match.ToString().Contains("600"))
                    urlsOfImages.Add(match.ToString());
            Console.WriteLine("Complete");
        }
    }
}
