﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CW2
{
    public static class Tester<T>
    {
        public static bool IsTestClass()
        {
            return typeof(T)
                .GetCustomAttributes(false)
                .OfType<TestFixtureAttributes>()
                .FirstOrDefault() != null;
        }
        
        public static void IfTestAttribute()
        {
            var type = typeof(T);
            var methods = type.GetMethods();
            foreach (var method in methods)
                if( method.GetCustomAttributes(false).OfType<TestAttributes>().FirstOrDefault() != null)
                {
                    try { method.Invoke(null, new object[] { });
                        Console.WriteLine("Test completed!");
                    }
                    catch { Console.WriteLine("Failed test!"); }
                }
        }

        public static void IfTearDownAttribute()
        {
            var type = typeof(T);
            var methods = type.GetMethods();
            foreach (var method in methods)
                if (method.GetCustomAttributes(false).OfType<TearDownAttributes>().FirstOrDefault() != null)
                {
                    try
                    {
                        method.Invoke(null, new object[] { });
                        Console.WriteLine("TearDown completed!");
                    }
                    catch { Console.WriteLine("Failed TearDown!"); }
                }
        }

        public static void IfSetUpAttribute()
        {
            var type = typeof(T);
            var methods = type.GetMethods();
            foreach (var method in methods)
                if (method.GetCustomAttributes(false).OfType<SetUpAttributes>().FirstOrDefault() != null)
                {
                    try
                    {
                        method.Invoke(null, new object[] { });
                        Console.WriteLine("SetUp completed!");
                    }
                    catch { Console.WriteLine("Failed set up!"); }
                }
        }
    }
}
