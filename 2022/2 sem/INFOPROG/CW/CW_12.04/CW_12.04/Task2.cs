﻿namespace CW_12._04
{
    public static class LinqExt
    {
        public static IEnumerable<Tuple<TItem, TItem>> Partition<TItem>(this IEnumerable<TItem> source, Func<TItem, bool> predicate)
        {
            var trueList = new List<TItem>();
            var falseList = new List<TItem>();
            foreach (var item in source)
            {
                if (predicate(item))
                    trueList.Add(item);
                else 
                    falseList.Add(item);
            }
            foreach (var item in trueList)
                foreach (var item2 in trueList)
                {
                    yield return Tuple.Create(item, item2);
                }
            foreach (var item in falseList)
                foreach (var item2 in falseList)
                { 
                        yield return Tuple.Create(item, item2);
                }

        }
    }
}
