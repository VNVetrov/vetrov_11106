﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BucketSortAISD
{
    public class LinkedList
    {
        public class Node
        {
            public int Value { get; set; }
            public Node? Next { get; set; }
        }

        public Node? Last { get; private set; }
        public Node? First { get; private set; }
        public int Count { get; private set; }

        public void Add(int item)
        {
            Node node = new Node() { Value = item, Next = null };
            if (First == null)
            {
                First = Last = node;
            }
            else
            {
                Last.Next = node;
                Last = node;
            }
            Count++;
        }

        public void AddRange(LinkedList list, ref int iterations)
        {
            for (int i = 0; i < list.Count; i++)
            {
                Add(list[i].Value);
                iterations++;
            }
        }

        public Node this[int index]
        {
            get
            {
                if (index > Count - 1 || index < 0) throw new Exception("Out of range");
                int counter = 0;
                Node current = First;
                while (current != null)
                {
                    if (counter == index)
                    {
                        return current;
                        break;
                    }
                    current = current.Next;
                    counter++;
                }
                return null;
            }
            set
            {
                if (index > Count - 1 || index < 0) throw new Exception("Out of range");
                int counter = 0;
                Node current = First;
                while (current != null)
                {
                    if (counter == index)
                    {
                        current = value;
                        break;
                    }
                    current = current.Next;
                    counter++;
                }
            }
        }

        public void Copy(LinkedList list)
        {
            this.First = list.First;
            this.Last = list.Last;
            Count = list.Count;
        }

        public static List<LinkedList> GetLinkedLists(List<int[]> arrays)
        {
            List<LinkedList> lists = new List<LinkedList>();
            foreach (int[] array in arrays)
            {
                var list = new LinkedList();
                foreach (int item in array)
                {
                    list.Add(item);
                }
                lists.Add(list);
            }
            return lists;
        }
    }
}
