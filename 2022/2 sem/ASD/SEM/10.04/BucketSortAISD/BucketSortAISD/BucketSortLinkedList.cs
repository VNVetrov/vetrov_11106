﻿namespace BucketSortAISD
{
    public static class BucketSortLinkedList
    {
        public static void BucketSort(LinkedList list, ref int iterations)
        {
            if (list.Count <= 1) return;
            int min = int.MaxValue, max = int.MinValue;
            var current = list.First;
            while (current != null)
            {
                if (current.Value > max) max = current.Value;
                if (current.Value < min) min = current.Value;
                current = current.Next;
                iterations++;
            }
            double range = (max - min);
            MyList<LinkedList> buckets = new MyList<LinkedList>(list.Count);
            for (int i = 0; i < buckets.Count; i++)
            {
                buckets[i] = new LinkedList();
                iterations++;
            }
            for (int i = 0; i < list.Count; i++)
            {
                int bucketIndex = (int)((list.Count-1)*(list[i].Value - min) / range);
                iterations += i;
                buckets[bucketIndex].Add(list[i].Value);
                iterations += i;
            }
            for (int i = 0; i < buckets.Count; i++)
            {
                InsertionSort(buckets[i], ref iterations);
            }
            var sortedList = new LinkedList();
            for (int i = 0; i < buckets.Count; i++)
            {
                sortedList.AddRange(buckets[i], ref iterations);

            }
            list.Copy(sortedList);
        }

        private static void InsertionSort(LinkedList list, ref int iterations)
        {
            for (int i = 1; i < list.Count; i++)
            {
                int value = list[i].Value;
                iterations += i;
                int j = i;
                while (j > 0 && list[j - 1].Value - value > 0)
                {
                    Swap(list, j, j - 1, ref iterations);
                    j -= 1;
                    iterations+=j-1;
                }
                list[j].Value = value;
                iterations += j;
            }
        }

        private static void Swap(LinkedList list, int i, int j, ref int iterations)
        {
            int temp = list[i].Value;
            list[i].Value = list[j].Value;
            list[j].Value = temp;
            iterations += 2 * i + 2 * j;
            
        }
    }
}
