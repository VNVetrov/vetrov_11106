﻿using System.Text;

namespace BucketSortAISD
{
    public static class IO
    {
        public static Tuple<List<int[]>, List<LinkedList>> ReadFile(int num)
        {
            string path = "Array" + num.ToString() + ".txt";
            var lines = File.ReadAllLines(path);
            var arrays = new List<int[]>();
            var lists = new List<LinkedList>();
            foreach (var line in lines)
            {
                var strNums = line.Trim().Split(' ');
                int[] intNums = new int[strNums.Length];
                var list = new LinkedList();
                for (int i = 0; i < strNums.Length; i++)
                {
                    intNums[i] = int.Parse(strNums[i]);
                    list.Add(int.Parse(strNums[i]));
                }
                arrays.Add(intNums);
                lists.Add(list);
            }
            
            return new (arrays, lists);
        }

        public static void Main()
        {
            if (FileGenerator.isCreated)
            {
                Console.WriteLine(@"Do you want to create new arrays? (y\n)");
                string? answer = Console.ReadLine();
                switch (answer)
                {
                    case "y":
                        FileGenerator.GenerateNewFiles();
                        Server.Run();
                        break;
                    case "n":
                        Server.Run();
                        break;
                    default:
                        Console.WriteLine("Wrong input!");
                        break;
                }
            }
            else
            {
                FileGenerator.GenerateNewFiles();
                Server.Run();
            }
        }

        public static void WriteFile()
        {
            string path = "Data.txt";
            var sb = new StringBuilder();
            sb.Append("\tArray\t|\tLinkedList\n" +
                "Lenght\tTicks|Iterations\tTicks|Iterations\n");
            var lines = new List<string>();
            lines.Add(sb.ToString());
            sb.Clear();
            foreach (var item in Server.ArrayTime)
            {
                var len = item.Key;
                int arrayIter = Server.ArrayIterations[len];
                int arrayTime = Server.ArrayTime[len];
                int linkedListIter = Server.LinkedListIterations[len];
                int linkedListTime = Server.LinkedListTime[len];
                sb.Append(len.ToString() + "\t" +
                    arrayTime.ToString() + " " +
                    arrayIter.ToString() + "\t" +
                    linkedListTime.ToString() + " " +
                    linkedListIter.ToString() + "\n");
                lines.Add(sb.ToString());
                sb.Clear();
            }
            File.WriteAllLines(path, lines);
            Console.WriteLine("{0} is located in {1}", path, Path.GetFullPath(path));
        }
    }
}
