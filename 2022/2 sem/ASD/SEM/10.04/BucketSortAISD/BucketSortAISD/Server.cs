﻿using System.Diagnostics;

namespace BucketSortAISD {
    public static class Server
    {
        public const int ArraysCount = 20;
        public const int AmountOfFiles = 100;
        public static Dictionary<int, int> ArrayIterations = new Dictionary<int, int>();
        public static Dictionary<int, int> ArrayTime = new Dictionary<int, int>();
        public static Dictionary<int, int> LinkedListIterations = new Dictionary<int, int>();
        public static Dictionary<int, int> LinkedListTime = new Dictionary<int, int>();

        public static void AnalyzeData(List<int[]> arrays, List<LinkedList> lists)
        {
            var timer = new Stopwatch();
            int lenght = arrays[0].Length;
            int iterations = 0;
            timer.Start();
            foreach (int[] array in arrays)
                BucketSortArray.BucketSort(array, ref iterations);
            timer.Stop();
            ArrayTime.Add(lenght, (int)timer.ElapsedMilliseconds / ArraysCount) ;
            ArrayIterations.Add(lenght, iterations / ArraysCount);
            iterations = 0;
            timer.Reset();
            timer.Start();
            foreach (var list in lists)
                BucketSortLinkedList.BucketSort(list, ref iterations);
            timer.Stop();
            LinkedListTime.Add(lenght, (int)timer.ElapsedMilliseconds / ArraysCount);
            LinkedListIterations.Add(lenght, iterations / ArraysCount);
            Console.WriteLine("{0} done!", lenght);
        }

        public static void Run()
        {
            for (int i = 0; i < AmountOfFiles; i++)
            {
                var tuple = IO.ReadFile(i);
                AnalyzeData(tuple.Item1, tuple.Item2);
            }
            IO.WriteFile();
        }
    } 
}