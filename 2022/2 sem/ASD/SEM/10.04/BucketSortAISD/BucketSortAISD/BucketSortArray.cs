﻿namespace BucketSortAISD
{
    public static class BucketSortArray
    {
        public static void BucketSort(int[] array, ref int iterations)
        {
            if (array.Length <= 1) return;
            int min = int.MaxValue, max = int.MinValue;
            foreach (var item in array)
            {
                if (item < min) min = item;
                if (item > max) max = item;
                iterations++;
            }
            double range = (max - min); 
            MyList<int>[] buckets = new MyList<int>[array.Length];//array.len
            for (int i = 0; i < array.Length; i++)//array.len
            {
                buckets[i] = new MyList<int>();
                iterations++;
            }
            foreach (var item in array)
            {
                int bucketIndex =(int)((array.Length-1)*(item - min) / range);//array.len - 1
                buckets[bucketIndex].Add(item);
                iterations++;
            }
            foreach (var bucket in buckets)
            {
                bucket.ToArray();
                InsertionSort(bucket.ArrayList, ref iterations);
            }
            var sortedArray = new MyList<int>();
            foreach (var bucket in buckets)
                sortedArray.AddRange(bucket.ArrayList);
            sortedArray.ToArray();
            Array.Copy(sortedArray.ArrayList, array, array.Length);
        }

        private static void Swap(int[] array, int i, int j)
        {
            int temp = array[i];
            array[i] = array[j];
            array[j] = temp;
        }

        private static void InsertionSort(int[] array, ref int iterations)
        {
            for (int i = 1; i < array.Length; i++)
            {
                int value = array[i];
                int j = i;
                while (j > 0 && array[j - 1] - value > 0)
                {
                    Swap(array, j, j - 1);
                    j -= 1;
                    iterations++;
                }
                array[j] = value;
            }
        }
    }
}