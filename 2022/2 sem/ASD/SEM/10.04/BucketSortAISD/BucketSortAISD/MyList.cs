﻿namespace BucketSortAISD
{
    public class MyList<T>
    {
        public T[] ArrayList;
        public int Count { get; private set; }

        public MyList(int count = 0)
        {
            ArrayList = new T[count];
            Count = count;
        }

        public void Add(T item)
        {
            if (Count == ArrayList.Length)
            {
                T[] newList = null;
                if (Count == 0)
                    newList = new T[1];
                else
                    newList = new T[2 * ArrayList.Length];
                Array.Copy(ArrayList, newList, ArrayList.Length);
                ArrayList = newList;
            }
            ArrayList[Count] = item;
            Count++;
        }

        public void ToArray()
        {
            var array = new T[Count];
            Array.Copy(ArrayList, array, Count);
            ArrayList = array;
        }

        public void AddRange(T[] array)
        {
            foreach (var item in array)
            {
                Add(item);
            }
        }

        public T this[int index]
        {
            get
            {
                return ArrayList[index];
            }
            set
            {
                ArrayList[index] = value;
            }
        }
    }
}
