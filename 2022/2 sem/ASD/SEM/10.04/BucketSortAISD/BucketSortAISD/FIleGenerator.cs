﻿using System.Text;
namespace BucketSortAISD
{
    public static class FileGenerator
    {
        public const int Step = 100;
        public static int Lenght = 100;
        public static bool isCreated = File.Exists("Data.txt");

        public static void GenerateNewFiles()
        {
            Random random = new Random();
            var sb = new StringBuilder();
            for (int i = 0; i < Server.AmountOfFiles; i++)
            {
                for (int j = 0; j < Server.ArraysCount; j++)
                {
                    for (int k = 0; k < Lenght; k++)
                        sb.Append(random.Next(-500, 500).ToString() + " ");
                    sb.Append("\n");
                }
                File.WriteAllText("Array" + i.ToString() + ".txt", sb.ToString());
                sb.Clear();
                Lenght += Step;
            }
        }
    }
}