﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treap_AISD
{
    public class Task
    {
        private static Random random = new Random();
        public static void Run(int size, int k)
        {
            var array = GetRandomArray(size);
            IO.OutputArray(array);
            var treap = CreateTreap(array);
            int answer = GetKthMax(treap, k);
        }

        private static Treap CreateTreap(int[] array)
        {
            var treap = new Treap(array[0], 0);
            for (int i = 1; i < array.Length; i++)
            {
                treap = treap.Add(array[i]);
            }
            return treap;
        }

        private static int[] GetRandomArray(int size)
        {
            int[] array = new int[size];
            for (int i = 0; i < array.Length; i++)
                array[i] = random.Next(0, 100);
            return array;
        }

        private static int GetKthMax(Treap treap, int k) 
        {
            for (int i = 0; i < k-1; i++)
            {
                int currentMax = treap.FindMax();
                Console.WriteLine("{0} max is {1}", i + 1, currentMax);
                treap = treap.Remove(currentMax);
            }
            int answer = treap.FindMax();
            Console.WriteLine("{0} max is {1}", k, answer);
            return answer;
        }
    }
}
