﻿namespace Treap_AISD {
    public class Program
    {
        public static void Main()
        {
            string[] input;
            int size, k;
            IO.InputData(out input, out size, out k);
            Task.Run(size, k);
        }
    } 
}