﻿
namespace Treap_AISD
{
    public static class RandomNumber
    {
        public static Random rand = new Random();
    }
    public class Treap
    {
        public int x, y;
        public Treap Left, Right;

        private Treap(int x, int y, Treap left = null, Treap right = null)
        {
            this.x = x;
            this.y = y;
            this.Left = left;
            this.Right = right;
        }

        public Treap(int x, int y) { this.x = x; this.y = y; }

        public static Treap Merge(Treap L, Treap R)
        {
            if (L == null) return R;
            if (R == null) return L;

            if (L.y > R.y)
            {
                var newR = Merge(L.Right, R);
                return new Treap(L.x, L.y, L.Left, newR);
            }
            else
            {
                var newL = Merge(L, R.Left);
                return new Treap(R.x, R.y, newL, R.Right);
            }
        }

        public void Split(int x, out Treap L, out Treap R)
        {
            Treap newTree = null;
            if (this.x <= x)
            {
                if (Right == null)
                    R = null;
                else
                    Right.Split(x, out newTree, out R);
                L = new Treap(this.x, y, Left, newTree);
            }
            else
            {
                if (Left == null)
                    L = null;
                else
                    Left.Split(x, out L, out newTree);
                R = new Treap(this.x, y, newTree, Right);
            }
        }

        public Treap Add(int x)
        {
            Treap l, r;
            Split(x, out l, out r);
            Treap m = new Treap(x, RandomNumber.rand.Next(0, 150));
            return Merge(Merge(l, m), r);
        }

        public Treap Remove(int x)
        {
            Treap l, m, r;
            Split(x - 1, out l, out r);
            r.Split(x, out m, out r);
            return Merge(l, r);
        }

        public bool Contains(int x)
        {
            if (this.x == x) return true;
            if (this.x > x && Left != null) return Left.Contains(x);
            if (this.x < x && Right != null) return Right.Contains(x);
            return false;
        }

        public int FindMax()
        {
            int max = int.MinValue;
            Treap current;
            current = this;
            while (current != null)
            {
                if (max < current.x)
                    max = current.x;
                current = current.Right;
            }
            return max;
        }
    }
}
