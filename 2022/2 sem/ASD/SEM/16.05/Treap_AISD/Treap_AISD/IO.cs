﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Treap_AISD
{
    public class IO
    {
        public static void InputData(out string[] input, out int size, out int k)
        {
            Console.WriteLine("Input array size and k:");
            input = Console.ReadLine().Split();
            size = int.Parse(input[0]);
            k = int.Parse(input[1]);
        }

        public static void OutputArray(int[] array)
        {
            Console.WriteLine("Array: \n");
            foreach (var item in array)
                Console.Write("{0} ", item);
            Console.WriteLine("\n");
        }
    }
}
