using NUnit.Framework;
using Treap_AISD;

namespace TreapTest
{
    public class Tests
    {

        [Test]
        public static void AddAndContainsTest()
        {
            var treap = new Treap(0, 0);
            for (int i = 1; i < 100; i++)
            {
                treap = treap.Add(i);
            }
            for (int i = 0; i < 100; i++)
            {
                Assert.IsTrue(treap.Contains(i));
            }
        }

        [Test]
        public static void ContainsWhenNotFound()
        {
            var treap = new Treap(0, 0);
            treap = treap.Add(1);
            treap = treap.Add(2);
            treap = treap.Add(3);
            treap = treap.Add(4);
            Assert.IsFalse(treap.Contains(6));
            Assert.IsFalse(treap.Contains(-1));
        }

        [Test]
        public static void RemoveTest()
        {
            var treap = new Treap(0, 0);
            treap = treap.Add(1);
            treap = treap.Remove(0);
            Assert.AreEqual(true, treap.Contains(1));
        }
    }
}