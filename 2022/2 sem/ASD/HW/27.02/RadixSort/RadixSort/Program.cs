﻿using System.Text;

public class Program
{
    public static void Main()
    {
    }

    public static string[] RadixSort(string[] elements)
    {
        int maxLength = 0;
        string[] answer = new string[elements.Length];
        List<char> symbols = new List<char>();
        Dictionary<char, MyQueue> dict = new Dictionary<char, MyQueue>();
        MyQueue bigQueue = new MyQueue();
        elements = PrepareForRadixSort(elements, ref maxLength, ref symbols);
        AddSortedSymbols(symbols, dict);
        for (int i = 0; i < elements.Length; i++)
        {
            bigQueue.Enqueue(elements[i]);
        }
        for (int index = maxLength - 1; index > -1; index--)
        {
            while (bigQueue.Count > 0)
            {
                string element = bigQueue.Dequeue();
                dict[element[index]].Enqueue(element);
            }
            bigQueue.JoinAllQueues(dict);
        }
        for (int i = 0; i < elements.Length; i++)
        {
            answer[i] = bigQueue.Dequeue();
        }
        DeleteSpaces(answer);
        return answer;
    }

    private static void DeleteSpaces(string[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            array[i] = array[i].Trim();
        }
    }
    private static void AddSortedSymbols(List<char> symbols, Dictionary<char, MyQueue> dict)
    {
        dict.Add(' ', new MyQueue());
        foreach (char symbol in symbols)
        {
            if (!dict.ContainsKey(symbol))
            {
                dict.Add(symbol, new MyQueue());
            }
        }
    }

    private static string[] PrepareForRadixSort(string[] elements, ref int maxLength,ref List<char> symbols)
    {
        StringBuilder sb = new StringBuilder();
        foreach (var item in elements)
        {
            if (item.Length > maxLength)
            {
                maxLength = item.Length;
            }
        }
        SortingSymbol(elements, symbols);
        for (int i = 0; i < elements.Length; i++)
        {
            for (int j = 0; j < maxLength - elements[i].Length; j++)
            {
                sb.Append(" ");
            }
            sb.Append(elements[i]);
            elements[i] = sb.ToString();
            sb.Clear();
        }
        return elements;
    }

    private static void SortingSymbol(string[] elements, List<char> symbols)
    {
        for (int i = 0; i < elements.Length; i++)
        {
            for (int j = 0; j < elements[i].Length; j++)
            {
                if (!symbols.Contains(elements[i][j]))
                {
                    symbols.Add(elements[i][j]);
                }
            }
        }
        symbols.Sort();
    }
}

public class MyQueue
{
    class Node
    {
        public string? Value { get; set; }
        public Node? Next { get; set; }
    }

    private Node? first;
    private Node? last;
    public int Count { get; private set; }
    public bool IsEmpty { get; private set; }
    public void Enqueue(string item)
    {
        Node? node = new Node() { Next = null, Value = item };
        Count++;
        IsEmpty = false;
        if (first == null)
        {
            first = last = node;
        }
        else
        {
            last.Next = node;
            last = node;
        }
    }

    public string Dequeue()
    {
        if (first == null) throw new InvalidOperationException("Empty queue");
        string result = first.Value;
        first = first.Next;
        if (first == null) last = null;
        Count--;
        if (Count == 0) IsEmpty = true;
        return result;
    }

    public void Clear()
    {
        last = first = null;
        Count = 0;
    }

    public void JoinAllQueues(Dictionary<char, MyQueue> dict)
    {
        foreach (var kvp in dict)
        {
            if (IsEmpty == true && kvp.Value.first != null)
            {
                first = kvp.Value.first;
                last = kvp.Value.last;
                IsEmpty = false;
                Count += kvp.Value.Count;
            }
            else if (kvp.Value.first != null)
            {
                last.Next = kvp.Value.first;
                last = kvp.Value.last;
                Count += kvp.Value.Count;
            }
            kvp.Value.Clear();
        }
    }
}

