using NUnit.Framework;

namespace RadixTests
{
    public class Tests
    { 
        [TestCase(new string[] {"1","123","122","2345","12"}, new string[] {"1","12","122","123","2345"})]
        [TestCase(new string[] {"1","2","3","4","5","6"}, new string[] { "1", "2", "3", "4", "5", "6" })]
        [TestCase(new string[] {"88","77","23","123123"},new string[] {"23","77","88","123123"})]
        [TestCase(new string[] {"1234","1324","1423","1432","2341","2314","3124","3241"}, new string[] {"1234","1324","1423","1432","2314","2341","3124","3241"})]
        public void Test(string[] expected, string[] answer)
        {
            expected = Program.RadixSort(expected);
            Assert.AreEqual(expected, answer);
        }
    }
}