﻿class Program
{
    public static void Main()
    {
    }

    public static int FacotrialBySimpModule(int n, int p)
    {
        int result = 1;
        while (n > 0)
        {
            int x = n / p;
            int y = n % p;
            if (x % 2 == 1)
                result = result * (p - 1) % p;
            for (int i = 1; i <= y; i++)
                result = result * i % p;
            n = x;
        }
        return result;
    }
}