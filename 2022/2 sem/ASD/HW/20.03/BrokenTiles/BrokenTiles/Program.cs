﻿class Program
{
    public static void Main()
    {
        var input = Console.ReadLine().Split(" ");
        int a = int.Parse(input[0]);
        int b = int.Parse(input[1]);
        var n = NOD(a, b);
        Console.WriteLine(a+b-n);
    }

    public static int NOD(int a, int b)
    {
        while (a != 0 && b!=0)
        {
            if (a >= b)
                a%=b;
            else
                b%=a;
        }
        return b != 0 ? b : a;
    }
}