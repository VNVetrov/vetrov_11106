using NUnit.Framework;

namespace TestConnectivity
{
    public class Tests
    {
        [Test]
        public void UsualTest()
        {
            var list = new ADS.Connectivity();
            list.Union("1-2");
            list.Union("3-4");
            list.Union("2-3");
            Assert.AreEqual("1-2-3-4", list.Find("1-2"));
            Assert.AreEqual("1-2-3-4", list.Find("1-3"));
            Assert.AreEqual("1-2-3-4", list.Find("1-4"));
            Assert.AreEqual("1-2-3-4", list.Find("2-4"));
            Assert.AreEqual("1-2-3-4", list.Find("1-1"));
        }

        [Test]
        public void NoConnectionTest()
        {
            var list = new ADS.Connectivity();
            list.Union("1-2");
            list.Union("3-4");
            Assert.AreEqual(list.Find("2-3"), "");
        }

        [Test]
        public void TwoComponentsTest()
        {
            var list = new ADS.Connectivity();
            list.Union("1-2");
            list.Union("3-4");
            list.Union("2-3");
            list.Union("5-6");
            list.Union("5-7");
            Assert.AreEqual(list.Find("2-3"), "1-2-3-4");
            Assert.AreEqual(list.Find("1-5"), "");
            Assert.AreEqual(list.Find("5-6"), "5-6-7");
        }
    }
}