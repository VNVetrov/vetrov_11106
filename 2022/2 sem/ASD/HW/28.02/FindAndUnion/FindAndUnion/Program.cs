﻿using System.Text;

namespace ADS 
{
    public class Program
    {
        public static void Main()
        {
        }
    }

    public class Connectivity
    {
        class Node
        {
            public int Value { get; set; }
            public Node? Next { get; set; }
        }

        class LinkedList
        {
            public Node? Last { get; set; }
            public Node? First { get; set; }
            public int Count { get; set; }

            public void Add(int item)
            {
                Node node = new Node() { Value = item, Next = null };
                if (First == null)
                {
                    First = Last = node;
                }
                else
                {
                    Last.Next = node;
                    Last = node;
                }
                Count++;
            }
        }

        private Dictionary<int, LinkedList> map = new Dictionary<int, LinkedList>();

        public void Union(string pair) // O(map[smallerElement].Count) =
                                       // = O(Size of the smaller connectivity component) ~
                                       // ~ O(n)
        {
            int firstElement, lastElement, biggerElement, smallerElement;
            PrepareDict(pair, out firstElement, out lastElement);
            if (map[firstElement].Count >= map[lastElement].Count)
            {
                biggerElement = firstElement;
                smallerElement = lastElement;
            }
            else
            {
                biggerElement = lastElement;
                smallerElement = firstElement;
            }
            if (map[smallerElement] == map[biggerElement]) return;
            ConnectLists(smallerElement, biggerElement);
        }

        private void ConnectLists(int smallerElement, int biggerElement)
        {
            map[biggerElement].Last.Next = map[smallerElement].First;
            map[biggerElement].Last = map[smallerElement].Last;
            map[biggerElement].Count = map[smallerElement].Count = map[biggerElement].Count + map[smallerElement].Count;
            var startNode = map[smallerElement].First;
            map[smallerElement].First = map[biggerElement].First;
            map[smallerElement] = map[biggerElement];
            UnionElements(biggerElement, startNode);
        }

        private void UnionElements(int bigger, Node? start)
        {
            var current = start;
            while (current != null)
            {
                map[current.Value] = map[bigger];
                current = current.Next;
            }
        }

        public string Find(string pair) // Find O(1) & Output O(n)
        {
            int firstElement, lastElement;
            PrepareDict(pair, out firstElement, out lastElement);
            if (map[firstElement] == map[lastElement])
            {
                return Output(firstElement);
            }
            return "";
        }

        private string Output(int firstElement)
        {
            var sb = new StringBuilder();
            Node? current = map[firstElement].First;
            while (current != null)
            {
                if (current.Next == null)
                {
                    sb.Append(current.Value.ToString());
                }
                else
                {
                    sb.Append(current.Value.ToString());
                    sb.Append("-");
                }
                current = current.Next;
            }
            return sb.ToString();
        }

        private void PrepareDict(string pair, out int firstElement, out int lastElement)
        {
            var elements = pair.Split('-');
            firstElement = int.Parse(elements[0]);
            lastElement = int.Parse((elements[1]));
            ContainsLinkedList(map, firstElement);
            ContainsLinkedList(map, lastElement);
        }

        private void ContainsLinkedList(Dictionary<int, LinkedList> map, int element)
        {
            if (!map.ContainsKey(element))
            {
                map.Add(element, new LinkedList());
                map[element].Add(element);
            }
        }
    }
}