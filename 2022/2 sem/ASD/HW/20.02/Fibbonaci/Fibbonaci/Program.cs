﻿using System.Diagnostics;


public class Program
{
    public static void Main()
    {
        List<long> listIterative, listMatrix, listRecursion;
        MeasureFibTime(out listIterative, out listMatrix, out listRecursion);
        DrawTimerTable(listIterative, listMatrix, listRecursion);
    }

    private static void MeasureFibTime(out List<long> listIterative, out List<long> listMatrix, out List<long> listRecursion)
    {
        Stopwatch timer1 = new Stopwatch();
        Stopwatch timer2 = new Stopwatch();
        Stopwatch timer3 = new Stopwatch();
        listIterative = new List<long>();
        listMatrix = new List<long>();
        listRecursion = new List<long>();
        for (int i = 0; i < 36; i++)
        {
            timer1.Start();
            FibByCycle(i);
            timer1.Stop();
            timer2.Start();
            FibByMatrix(i);
            timer2.Stop();
            timer3.Start();
            FibByRecursion(i);
            timer3.Stop();
            listIterative.Add(timer1.ElapsedMilliseconds);
            listMatrix.Add(timer2.ElapsedMilliseconds);
            listRecursion.Add(timer3.ElapsedMilliseconds);
        }
    }

    private static void DrawTimerTable(List<long> listIterative, List<long> listMatrix, List<long> listRecrsion)
    {
        Console.WriteLine("  N\tTime | Iterative\tMatrix\t    Recursion");
        for (int i = 0; i < 36; i++)
        {
            Console.WriteLine("{0,4}{1,15}{2,15}{3,15}", i,
                listIterative[i], 
                listMatrix[i],
                listRecrsion[i]);
        }
    }

    //Iterative
    public static int FibByCycle(int n)
    {
        int a = 0, b = 1, temp;
        for (int i = 0; i < n; i++)
        {
            temp = b;
            b = a + b;
            a = temp;
        }
        return a;
    }
    //Recursion
    public static int FibByRecursion(int n)
    {
        if (n < 2) return n;
        else return FibByRecursion(n - 2) + FibByRecursion(n - 1);
    } 
    //Matrix
    public static int FibByMatrix(int n)
    {
        var unitMatrix = UnitMatrix(2);
        var firstMatrix = new int[2, 2]
        {
            {1 ,1},
            {1 ,0}
        };
        int[,] result = MatrixPow(firstMatrix, n, unitMatrix, firstMatrix);
        return result[0,1];
    }

    public static int[,] MatrixPow(int[,] matrix, int n, int[,] unitMatrix, int[,] originMatrix)
    {
        if (n == 0)
        {
            return unitMatrix;
        }
        else if (n % 2 == 0)
        {
            var currentMatrix = MatrixPow(matrix, n / 2, unitMatrix, originMatrix);
            return MatrixMultiply(currentMatrix, currentMatrix);
        }
        else
        {
            return  MatrixMultiply(originMatrix, MatrixPow(matrix, n - 1, unitMatrix, originMatrix));
        }
    }

    public static int[,] UnitMatrix(int n)
    {
        var unitMatrix = new int[n, n];
        for (int i = 0; i < n; i++)
        {
            for (int j = 0; j < n; j++)
            {
                if (i == j) unitMatrix[i, j] = 1;
                else unitMatrix[i, j] = 0;
            }
        }
        return unitMatrix;
    }

    public static int[,] MatrixMultiply(int[,] firstMatrix, int[,] secondMatrix)
    {
        int firstRow = firstMatrix.GetLength(0);
        int secondCol = secondMatrix.GetLength(1);
        int firstCol = firstMatrix.GetLength(1);
        int[,] result = new int[firstRow,secondCol];
        for (int i = 0; i < firstRow; i++)
        {
            for (int j = 0; j < secondCol; j++)
            {
                for (int k = 0; k < firstCol; k++)
                {
                    result[i, j] += firstMatrix[i, k] * secondMatrix[k, j];
                }
            }
        }
        return result;
    }
}

