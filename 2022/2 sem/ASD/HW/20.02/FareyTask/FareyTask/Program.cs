﻿public class Node
{
    public int Numerator;
    public int Denominator;
    public int NextIndex;

    public override string ToString()
    {
        return String.Format("{0}/{1}", Numerator, Denominator);
    }
}
namespace Farey
{
    public class Program
    {
        public static void Main()
        {
            var array = GetFareyArray(2);
            foreach (var item in array)
            {
                Console.WriteLine(item);
            }
        }

        public static Node[] GetFareyArray(int n)
        {
            var array = new Node[n * n];
            array[0] = new Node { Numerator = 0, Denominator = 1, NextIndex = 1 };
            array[1] = new Node { Numerator = 1, Denominator = 1, NextIndex = -1 };
            int lastAble = 2;
            for (int i = 2; i <= n; i++)
            {
                Node current = array[0];
                while (current.NextIndex != -1)
                {
                    var next = current.NextIndex;
                    if (current.Denominator + array[current.NextIndex].Denominator == i)
                    {
                        array[lastAble] = new Node { Numerator = current.Numerator + array[next].Numerator, Denominator = i, NextIndex = next };
                        current.NextIndex = lastAble++;
                    }
                    current = array[next];
                }
            }
            //Sorting
            var newArray = new Node[lastAble];
            newArray[0] = array[0];
            Node actual = array[0];
            for (int i = 1; i < lastAble; i++)
            {
                newArray[i] = array[actual.NextIndex];
                actual = array[actual.NextIndex];
            }
            return newArray;
        }
    }
}