using NUnit.Framework;
using Farey;
using System.Text;

namespace FareyTests
{
    public class Tests
    {
        [TestCase(2, "0/1,1/2,1/1")]
        [TestCase(3,"0/1,1/3,1/2,2/3,1/1")]
        [TestCase(6,"0/1,1/6,1/5,1/4,1/3,2/5,1/2,3/5,2/3,3/4,4/5,5/6,1/1")]
        public void FareyTest(int n, string answer)
        {
            var array = Program.GetFareyArray(n);
            var sb = new StringBuilder();
            for (int i = 0; i < array.Length; i++)
            {
                if (i != array.Length - 1)
                    sb.Append(array[i].ToString() + ",");
                else
                    sb.Append(array[i].ToString());
            }
            Assert.AreEqual(answer, sb.ToString());
        }
    }
}