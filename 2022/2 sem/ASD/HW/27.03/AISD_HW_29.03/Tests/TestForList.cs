using MatrixLinkedListNS;
using NUnit.Framework;
using System;
using System.IO;
using System.Text;

namespace Tests
{
    public class Tests
    {
        //Method Tests
        [Test]
        public void TestEncodeAndDecode()
        {
            var matrix = GetRandomMatrix();
            var list = new MatrixLinkedList();
            list.Encode(matrix);
            list.Decode();
            var answer = File.ReadAllText("Matrix.txt");
            var strList = new List<string>();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    strList.Add(matrix[i, j].ToString());
                    strList.Add("\t");
                }
                strList.Add("\n");
            }
            Assert.AreEqual(strList.ToString(), answer);
        }

        [Test]
        public void TestRemove()
        {
            var rnd = new Random();
            var matrix = GetRandomMatrix();
            var list = new MatrixLinkedList();
            list.Encode(matrix);
            int toRemoveRow = rnd.Next(0, list.Dimension - 1);
            int toRemoveColumn = rnd.Next(0, list.Dimension - 1);
            list.Remove(toRemoveRow, toRemoveColumn);
            matrix = list.Decode();
            Assert.AreEqual(matrix[toRemoveRow, toRemoveColumn], 0);
        }

        [Test]
        public void TestInsert()
        {
            var rnd = new Random();
            var matrix = GetRandomMatrix();
            var list = new MatrixLinkedList();
            list.Encode(matrix);
            int toInsertRow = rnd.Next(0,list.Dimension - 1);
            int toInsertColumn = rnd.Next(0,list.Dimension - 1);
            int toInsertValue = rnd.Next(0, 100);
            list.Insert(toInsertValue, toInsertRow, toInsertColumn);
            matrix = list.Decode();
            Assert.AreEqual(matrix[toInsertRow, toInsertColumn], toInsertValue);
        }

        [Test]
        public void TestGetMinValuesOfColumn()
        {
            var matrix = GetRandomMatrix();
            var list = new MatrixLinkedList();
            list.Encode(matrix);
            int[] answer = list.GetMinValuesOfColumns();
            int[] expected = new int[list.Dimension];
            for (int i = 0; i < list.Dimension; i++)
                expected[i] = int.MaxValue;
            for (int i = 0; i < list.Dimension; i++)
                for (int j = 0; j < list.Dimension; j++)
                    if (matrix[i, j] <= expected[j])
                        expected[j] = matrix[i, j];
            Assert.AreEqual(expected,answer);
        }

        [Test]
        public void TestGetDiagonalSum()
        {
            int[,] matrix = GetRandomMatrix();
            var list = new MatrixLinkedList();
            list.Encode(matrix);
            int mainDiagonalSum = list.GetDiagonalSum(Diagonals.Main);
            int sideDiagonalSum = list.GetDiagonalSum(Diagonals.Side);
            int expectedMain = 0, expectedSide = 0;
            for (int i = 0; i < list.Dimension; i++)
            {
                expectedMain+=matrix[i,i];
                expectedSide += matrix[i, list.Dimension - i - 1];
            }
            Assert.AreEqual(expectedSide, sideDiagonalSum);
            Assert.AreEqual(expectedMain, mainDiagonalSum);
        }

        [Test]
        public void TestTranspose()
        {
            int[,] matrix = GetRandomMatrix();
            var list = new MatrixLinkedList();
            list.Encode(matrix);
            list.Transpose();
            var trMatrix = list.Decode();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = i; j < matrix.GetLength(1); j++)
                {
                    var temp = matrix[i, j];
                    matrix[i, j] = matrix[j, i];
                    matrix[j, i] = temp;
                }
            }
            Assert.AreEqual(trMatrix, matrix);
        }

        [Test]
        public void TestSumTwoColumns()
        {
            Random rand = new Random();
            int[,] matrix = GetRandomMatrix();
            var str = new StringBuilder();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int k = 0; k < matrix.GetLength(1); k++)
                    str.Append(matrix[i, k].ToString() + " ");
                str.Append("\n");
            }
            var list = new MatrixLinkedList();
            list.Encode(matrix);
            int firstColumn = rand.Next(0, list.Dimension-1);
            int secondColumn = rand.Next(0, list.Dimension-1);
            list.SumTwoColumns(firstColumn, secondColumn);
            int[] answer = new int[list.Dimension];
            int[] expected = new int[list.Dimension];
            var newMatrix = list.Decode();
            for (int i = 0; i < matrix.GetLength(0); i++)
                answer[i] = newMatrix[i, firstColumn];
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    if (firstColumn != secondColumn)
                    {
                        if (j == firstColumn || j == secondColumn)
                            expected[i] += matrix[i, j];
                    }
                    else if (j == firstColumn)
                    {
                        expected[i] = matrix[i, j] * 2;
                    }
                }
            }
            Assert.AreEqual(expected, answer);
            Assert.Throws<ArgumentOutOfRangeException> (() => list.SumTwoColumns(-1,0));
            Assert.Throws<ArgumentOutOfRangeException>(() => list.SumTwoColumns(-1, 100));
        }

        [Test]
        public void MainTest()
            //We need this test because tests below require random matrix.
        {
            for (int i = 0; i < 1000; i++)
            {
                TestInsert();
                TestRemove();
                TestGetDiagonalSum();
                TestGetMinValuesOfColumn();
                TestSumTwoColumns();
                TestTranspose();
            }
        }

        public int[,] GetRandomMatrix()
        {
            Random rnd = new Random();
            int dimension = rnd.Next(2,10);
            int[,] matrix = new int[dimension,dimension];
            for (int i = 0; i < dimension; i++)
                for (int j = 0; j < dimension; j++)
                    matrix[i, j] = rnd.Next(0, 20);
            return matrix;
        }
    }
}