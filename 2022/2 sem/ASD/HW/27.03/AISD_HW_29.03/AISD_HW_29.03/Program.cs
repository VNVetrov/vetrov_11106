﻿using System;
using System.Text;

namespace MatrixLinkedListNS
{
    public class Program
    {
        public static void Main()
        {
            Server.DoServerWork();
        }
    }

    /// <summary>
    /// Enumeration of types of diagonals.
    /// </summary>
    public enum Diagonals
    {
        Main,
        Side
    }

    /// <summary>
    /// A list whose elements contain, row number,
    /// column number and the value of a non-zero component.
    /// </summary>
    public class MatrixLinkedList
    {
        /// <summary>
        /// A node of a linked list.
        /// </summary>
        class Node
        {
            public int Value { get; set; }
            public int Row { get; set; }
            public int Column { get; set; }
            public Node? Next { get; set; }
        }

        /// <summary>
        /// A number showing the dimension of the matrix.
        /// </summary>
        public int Dimension { get; private set; }
        private Node? Last { get; set; }
        private Node? First { get; set; }

        /// <summary>
        /// Adding a node to a linked list.
        /// </summary>
        /// <param name="value">Value</param>
        /// <param name="row">Row</param>
        /// <param name="column">Column</param>
        private void Add(int value, int row, int column)
            // Time and space complexity: O(1)
        {
            Node? newNode = new() { Value = value, Row = row, Column = column };
            if (First == null)
            {
                Last = First = newNode;
            }
            else
            {
                Last.Next = newNode;
                Last = newNode;
            }
        }
        
        /// <summary>
        /// Creating a list of non-zero elements based on
        /// a matrix specified by an array containing zero elements.
        /// </summary>
        /// <param name="matrix">Matrix array.</param>
        public void Encode(int[,] matrix)
            //Time complexity: O(n)
            //Space complexity: O(1)
        {
            Dimension = matrix.GetLength(0);
            for (int i = 0; i < Dimension; i++)
                for (int j = 0; j < Dimension; j++)
                    if (matrix[i, j] != 0)
                        Add(matrix[i, j], i, j);
        }

        /// <summary>
        /// Restoring the original matrix with the output
        /// of the result to a text file, with the release
        /// of the allocated dynamic memory.
        /// </summary>
        /// <returns>Matrix array.</returns>
        public int [,] Decode()
        //The initial movement of elements from
        //the list to the array, and then placing the matrix in a file.
        //O(n)
        {
            var output = new List<string>();
            var matrix = new int[Dimension, Dimension];
            Node? current = First;
            while (current != null)
            {
                matrix[current.Row, current.Column] = current.Value;
                current = current.Next;
            }
            for (int i = 0; i < Dimension; i++)
            {
                for (int j = 0; j < Dimension; j++)
                {
                    output.Add(matrix[i, j].ToString());
                    output.Add("\t");
                }
                output.Add("\n");
            }
            File.WriteAllText("Matrix.txt", output.ToString());
            Console.WriteLine("Matrix.txt located in {0}", Path.GetFullPath("Matrix.txt"));
            First = Last = null;
            Dimension = 0;
            return matrix;
        }

        /// <summary>
        /// Inserting a non-zero element into some position
        /// of the matrix, while if such a component existed
        /// in the vector, then this component is replaced.
        /// </summary>
        /// <param name="value">Value</param>
        /// <param name="row">Row</param>
        /// <param name="column">Column</param>
        /// <exception cref="ArgumentOutOfRangeException">In case of wrong row or column numbers.</exception>
        public void Insert(int value, int row, int column)
        //Placing an element in its position.
        //The position is determined by a pair of numbers (i,j),
        //where i is the row number, j is the column number.
        //If the item was already in the list,
        //its value is replaced with a new one.
        //To facilitate the work of other methods,
        //these pairs are sorted.
        //Time complexity: O(n)
        //Space complexity: O(1)
        {
            if (row < 0 || column < 0 || row > Dimension || column > Dimension)
                throw new ArgumentOutOfRangeException("Inserting out of the range of list.");
            if (value == 0)
            {
                Remove(row, column);
                Console.WriteLine("List contains only non-zero numbers.");
                return;
            }
            bool flag = false;
            Node? prev = null;
            Node? current = First;
            while (current != null)
            {
                if ((current.Row == row && current.Column > column) || current.Row > row)
                {
                    Node? newNode = new Node() { Value = value, Row = row, Column = column };
                    if (prev == null)
                    {
                        First = newNode;
                        newNode.Next = current;
                    }
                    else
                    {
                        prev.Next = newNode;
                        newNode.Next = current;
                    }
                    flag = true;
                    break;
                }
                else if (current.Row == row && current.Column == column)
                {
                    current.Value = value;
                    flag = true;
                    break;
                }
                prev = current;
                current = current.Next;
            }
            if (!flag)
                Add(value, row, column);
        }

        /// <summary>
        /// Zeroing an element of the matrix located in a certain position.
        /// </summary>
        /// <param name="row"></param>
        /// <param name="column"></param>
        /// <exception cref="ArgumentOutOfRangeException">In cause of wrong row or column number.</exception>
        public void Remove(int row, int column)
        //Search for an item in the list and delete it.
        //If the item was not originally in the list, then nothing will change.
        //Time complexity: O(n)
        //Space complexity: O(1)
        {
            if (row < 0 || column < 0 || row >= Dimension || column >= Dimension)
                throw new ArgumentOutOfRangeException("Removing out of the range of list.");
            Node? prev = null;
            Node? current = First;
            bool flag = false;
            while (current != null)
            {
                if (current.Row == row && current.Column == column)
                {
                    if (prev == null)
                    {
                        First = current.Next;
                        current.Next = null;
                    }
                    else
                    {
                        prev.Next = current.Next;
                        current.Next = null;
                    }
                    flag = true;
                    break;
                }
                else if (current.Row >= row && current.Column > column)
                {
                    break;
                }
                prev = current;
                current = current.Next;
            }
            if (!flag)
                Console.WriteLine("There is nothing to remove.");
        }

        /// <summary>
        /// Forming a list of the minimum elements of each column of the matrix.
        /// </summary>
        /// <returns>Array of minimum values.</returns>
        public int[] GetMinValuesOfColumns()
        //Finding the minimum of each column and
        //counting the number of elements in the column.
        //If the number of column elements is less
        //than the dimension of the matrix, then there is a zero component in the matrix.
        //Time complexity: O(n)
        //Space complexity: O(sqrt(n))
        {
            int[] mins = new int[Dimension];
            int[] columntCount = new int[Dimension];
            for (int i = 0; i < Dimension; i++)
                mins[i] = int.MaxValue;
            Node? current = First;
            while (current != null)
            {
                if (mins[current.Column] > current.Value)
                    mins[current.Column] = current.Value;
                columntCount[current.Column]++;
                current = current.Next;
            }
            for (int i = 0; i < Dimension; i++)
                if (columntCount[i] < Dimension && mins[i] > 0)
                    mins[i] = 0;
            return mins;
        }

        /// <summary>
        /// Calculating the sum of diagonal elements
        /// </summary>
        /// <param name="diagonal">Type of diagonal.</param>
        /// <returns>The sum of diagonal elements.</returns>
        public int GetDiagonalSum(Diagonals diagonal)
        //Goes through the entire list and summarizes
        //the elements belonging to the diagonal
        //Time complexity: O(n)
        //Space complexity: O(1)
        {
            int sum = 0;
            Node? current = First;
            while (current != null)
            {
                if ((diagonal == Diagonals.Main 
                    && current.Row == current.Column)
                    || (diagonal == Diagonals.Side 
                    && current.Row == Dimension - current.Column - 1))
                    sum += current.Value;
                current = current.Next;
            }
            return sum;
        }

        /// <summary>
        /// Matrix transposition.
        /// </summary>
        public void Transpose()
        //It goes through the entire list and
        //changes the row number with the column number
        //Time complexity: O(n)
        //Space complexity: O(1)
        {
            Node? current = First;
            while (current != null)
            {
                int temp = current.Row;
                current.Row = current.Column;
                current.Column = temp;
                current = current.Next;
            }
        }

        /// <summary>
        /// Sum two columns of the matrix with placing the result in the first column.
        /// </summary>
        /// <param name="column1">Number of the first column.</param>
        /// <param name="column2">Number of the second column.</param>
        /// <exception cref="ArgumentOutOfRangeException">In case of wrong column numbers.</exception>
        public void SumTwoColumns(int column1, int column2)
        //We go through the list, sum up the
        //sum of the columns in a special array
        //and then go through the list again and
        //insert the elements into the first column.
        //Time complexity: O(n*sqrt(n))
        //Space complexity: O(sqrt(n))
        {
            if (column1 < 0 || column1 >= Dimension
                || column2 < 0 || column2 >= Dimension)
                throw new ArgumentOutOfRangeException("Column out of the range.");
            int[] sumColumn = new int[Dimension];
            Node? current = First;
            while(current != null)
            {
                if (current.Column == column1 && column1 == column2)
                    sumColumn[current.Row] += current.Value*2;
                else if (current.Column == column1 || current.Column == column2)
                    sumColumn[current.Row] += current.Value;
                current = current.Next;
            }
            for (int i = 0;i< Dimension; i++)
                Insert(sumColumn[i], i, column1);
        }
    }
}