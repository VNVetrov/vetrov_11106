﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatrixLinkedListNS
{
    /// <summary>
    /// Dynamically expandable array.
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class List<T>
    {
        public T[] ArrayList;
        public int Count { get; private set; }

        public List()
        {
            ArrayList = new T[1];
            Count = 0;
        }

        /// <summary>
        /// Adding an item to the list.
        /// </summary>
        /// <param name="item">Item.</param>
        public void Add(T item)
        {
            if (Count == ArrayList.Length)
            {
                var newList = new T[2 * ArrayList.Length];
                Array.Copy(ArrayList, newList, ArrayList.Length);
                ArrayList = newList;
            }
            ArrayList[Count] = item;
            Count++;
        }

        /// <summary>
        /// Converting a list to a string.
        /// </summary>
        /// <returns>String format of a list.</returns>
        public override string ToString()
        {
            return String.Join('\0', ArrayList);
        }
    }
}
