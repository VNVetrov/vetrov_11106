﻿
namespace MatrixLinkedListNS
{
    public static class Server
    {
        public static void DoServerWork()
        {
            int[,] matrix;
            MatrixLinkedList list = new MatrixLinkedList();
            MatrixInput(out matrix, list);
            Console.WriteLine("Options:\nInsert\nRemove\n" +
                "Sum 2 columns\nFinish\nType option you want to do");
            bool isFinished = false;
            while (!isFinished)
            {
                var input = Console.ReadLine();
                switch (input)
                {
                    case "Insert":
                        MatrixInsert(matrix, list);
                        break;
                    case "Remove":
                        MatrixRemove(matrix, list);
                        break;
                    case "Sum 2 columns":
                        MatrixSumTwoColumns(matrix, list);
                        break;
                    case "Finish":
                        isFinished = true;
                        break;
                    default:
                        Console.WriteLine("Wrong input, type again!");
                        break;
                }
            }
        }

        private static void MatrixSumTwoColumns(int[,] matrix, MatrixLinkedList list)
        {
            Console.WriteLine("Type number of columns ex. 1 2");
            var sum = Console.ReadLine().Split();
            int first = int.Parse(sum[0]);
            int second = int.Parse(sum[1]);
            list.SumTwoColumns(first, second);
            matrix = list.Decode();
            list.Encode(matrix);
            MatrixOutput(matrix);
            Console.WriteLine("Success!");
        }

        private static void MatrixRemove(int[,] matrix, MatrixLinkedList list)
        {
            Console.WriteLine("Type row and column ex. 1 2");
            var remove = Console.ReadLine().Split();
            int row = int.Parse(remove[0]);
            int column = int.Parse(remove[1]);
            list.Remove(row, column);
            matrix = list.Decode();
            list.Encode(matrix);
            MatrixOutput(matrix);
            Console.WriteLine("Succes!");
        }

        private static void MatrixInsert(int[,] matrix, MatrixLinkedList list)
        {
            Console.WriteLine("Type value, row and column ex. 1 2 3 ");
            var insert = Console.ReadLine().Split();
            int value = int.Parse(insert[0]);
            int row = int.Parse(insert[1]);
            int column = int.Parse(insert[2]);
            list.Insert(value, row, column);
            matrix = list.Decode();
            list.Encode(matrix);
            MatrixOutput(matrix);
            Console.WriteLine("Succes!");
        }

        private static void MatrixInput(out int[,] matrix, MatrixLinkedList list)
        {
            Console.WriteLine("Set the dimension of matrix");
            var dimension = int.Parse(Console.ReadLine());
            Console.WriteLine("Input matrix");
            string[] rowInput;
            matrix = new int[dimension, dimension];
            for (int i = 0; i < dimension; i++)
            {
                rowInput = Console.ReadLine().Split(' ');
                for (int j = 0; j < dimension; j++)
                {
                    matrix[i, j] = int.Parse(rowInput[j]);
                }
            }
            list.Encode(matrix);
            Console.WriteLine("Succes!");
        }

        private static void MatrixOutput(int[,] matrix)
        {
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    Console.Write("{0} ", matrix[i, j]);
                }
                Console.WriteLine();
            }
        }
    }
}
