SELECT cnum, min(amt) as min_amt
FROM Orders 
GROUP BY cnum 
HAVING min(amt) > (SELECT avg(amt) FROM Orders)