SELECT o1.onum as onum_first,
	c1.cname as cname_first,
	o2.onum as onum_second,
	c2.cname as cname_second
FROM Orders o1 
	JOIN Customers c1 ON
	o1.cnum = c1.cnum, Orders o2 
	JOIN Customers c2 ON o2.cnum = c2.cnum
WHERE o1.onum != o2.onum and o1.onum < o2.onum