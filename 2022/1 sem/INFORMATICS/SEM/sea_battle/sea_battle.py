class Field:
    def __init__(self):
        self.field: set[tuple] = set()

    def is_hit(self, pos: tuple):
        return pos in self.field
