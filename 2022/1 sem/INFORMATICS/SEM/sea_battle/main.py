from PyQt6.QtWidgets import QMainWindow, QApplication

import first_view
import sea_battle


class WelcomeView(QMainWindow, first_view.Ui_MainWindow):
    def __init__(self):
        super(WelcomeView, self).__init__()
        self.setupUi(self)
        self.offline_button.clicked.connect(self.start_game_window)
        # TODO: make for online game

    def start_game_window(self):
        window.hide()
        game_setting_window = GameSettingView()
        game_setting_window.show()


class GameSettingView(QMainWindow):
    def __init__(self):
        super().__init__()


if __name__ == "__main__":
    app = QApplication([])
    window = WelcomeView()
    window.show()
    app.exec()
