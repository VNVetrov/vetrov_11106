create table users
(
    id       serial
        primary key,
    username varchar(255),
    balance  numeric    default 0,
    password varchar(20)                                         not null,
    role     varchar(7) default 'user'::character varying
);

alter table users
    owner to webshop;

create table currencies
(
    id          serial
        primary key,
    name        varchar(50)                                                not null,
    description text                                                       not null,
    cost_dollar numeric                                                    not null,
    owner_id    integer
        constraint currencies_user__fk
            references users,
    amount      numeric                                                    not null,
    logo        varchar(255) default 'no_logo_coin.png'::character varying not null
);

alter table currencies
    owner to webshop;

create table users_currencies
(
    user_id     integer not null
        constraint users_currencies_user__fk
            references users,
    currency_id integer not null
        constraint users_currencies_currencies__fk
            references currencies,
    count       numeric default 0
);

alter table users_currencies
    owner to webshop;

create table currency_to_dollar
(
    name           varchar(3)           not null
        unique,
    cost_to_dollar numeric    default 1 not null,
    mark           varchar(1) default '$'::character varying
);

alter table currency_to_dollar
    owner to webshop;

create table transaction_history
(
    currency_id integer
        constraint table_name_currencies__fk
            references currencies,
    seller_id   integer
        constraint table_name_users_id_seller__fk
            references users,
    buyer_id    integer
        constraint table_name_users_id_buyer__fk
            references users,
    amount      numeric,
    payed_sum   numeric
);

alter table transaction_history
    owner to webshop;

