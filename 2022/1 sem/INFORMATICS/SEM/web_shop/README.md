<h1 align="center">GitCoin P2P Platform Flask project</h1>
<img src="static/images/small_gitcoin_logo.png" alt="gitcoin_logo">
<ul>
<li> Linux used
<li> <code>python3 -m venv .venv</code> - create environment
<li> <code>source venv/local/bin/activate</code> - activate environment
<li> <code>docker-compose up -d</code> - installing db Postgresql
<li>  Run <code>SQL_create_tables.sql</code>
<li> <code>pip install -r requirements.txt</code> - installing packages
<li> <code>export FLASK_DEBUG=1/0</code> - turn on/off Flask debug 
<li> <code>flask --app main.py run</code> - run project
</ul>