import datetime
import os

from flask import *
from db_util import Database
from functools import wraps


# needed decorators
def login_required(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        if session.get('user', None) is None:
            return redirect(url_for('login_view', next=request.url))
        return func(*args, **kwargs)

    return wrapper


# import config
with open('config.json', 'r') as f:
    config = json.load(f)

# app settings
app = Flask(__name__)
app.secret_key = config['secret_key']
app.permanent_session_lifetime = datetime.timedelta(days=5)
app.config['UPLOAD_FOLDER'] = config['upload_folder']


# flask decorators
@app.template_filter('price_format')
def price_format_filter(value, idx_num: int = 7):
    value = str(value)
    second_part = []
    if '.' in value:
        first_part = list(value.split('.')[0])
        second_part = list(value.split('.')[1])
    else:
        first_part = list(value)
    first_part = first_part[::-1]
    first_part = make_commas(first_part)
    first_part = first_part[::-1]
    if second_part:
        second_part = make_commas(second_part)
        if len(second_part) > idx_num:
            second_part = second_part[:5]
        return ''.join(first_part + ['.'] + second_part) + '$'
    else:
        return ''.join(first_part) + '$'


# flask functions
@app.context_processor
def inject_user():
    username = session.get('user', None)
    if username:
        user = db.filter(table_name='users', username=username)[0]
    else:
        user = None
    return dict(is_authenticated=bool(username),
                currency_to_dollar=currency_to_dollar,
                user=user)


# connecting to db
db = Database()

# data needed to import only once
currency_to_dollar = db.select('currency_to_dollar')


# needed functions
def is_valid(data):
    return 0 < len(data) < 20 and ' ' not in data


def get_user():
    if session.get('user', None):
        user = db.filter(table_name='users', username=session.get('user', None))[0]
    else:
        user = None
    return user


def float_or_int(value):
    if '.' in str(value):
        return int(str(value)[:-2]) if str(value).endswith('.0') else float(value)
    return int(value)


def make_commas(lst):
    res = []
    for idx, symbol in enumerate(lst):
        if idx % 3 == 0 and idx != 0:
            res.append(',')
        res.append(symbol)
    return res


# views
@app.route("/register", methods=['get', 'post'])
def register_view():
    context = {"login": False,
               "message": "Register!"}
    if request.method == "POST":
        form = request.form
        username = form.get('username')
        password = form.get('password')
        if is_valid(username) and is_valid(password) \
                and not db.filter('users', username=username):
            db.insert('users', {'username': username,
                                'password': password,
                                'role': 'user'})
            session[username] = []
            context['message'] = "Successfully registered!"
        else:
            context['message'] = "Invalid username or password!"
    return render_template('auth_page.html', **context)


@app.route("/login", methods=['get', 'post'])
def login_view():
    context = {"login": True,
               "message": "Log in or register if you are a newbie!"}
    if request.method == "POST":
        form = request.form
        username = form.get('username')
        password = form.get('password')
        if is_valid(username) and is_valid(password) \
                and db.filter('users', username=username, password=password):
            session['user'] = username
            session[username] = session.get(username, [])
            return redirect("/market")
        else:
            context['message'] = "Invalid username or password!"
    return render_template('auth_page.html', **context)


@app.route("/")
def redirect_to_main():
    return redirect(url_for('main_view', next=request.url))


@app.route('/transaction_history')
@login_required
def history_view():
    transactions = db.select(table_name='transaction_history')
    users = db.select(table_name='users')
    currencies = db.select(table_name='currencies')
    id_to_username = dict()
    id_to_cur_info = dict()
    for user in users:
        id_to_username[int(user['id'])] = user['username']
    for currency in currencies:
        id_to_cur_info[int(currency['id'])] = (currency['name'], currency['logo'])
    for transaction in transactions:
        transaction['seller'] = id_to_username[int(transaction['seller_id'])]
        transaction['buyer'] = id_to_username[int(transaction['buyer_id'])]
        transaction['currency_name'] = id_to_cur_info[int(transaction['currency_id'])][0]
        transaction['currency_logo'] = id_to_cur_info[int(transaction['currency_id'])][1]
        transaction['amount'] = float_or_int(transaction['amount'])

    return render_template('transactions_page.html', transactions=transactions)


@app.post("/add_currency")
@login_required
def add_currency_post_view():
    form_data = request.form
    logo = request.files['logo']
    user_id = get_user().get('id')
    if logo:
        logo.save(os.path.join(app.config['UPLOAD_FOLDER'], logo.filename))
    new_currency = {
        'name': form_data['name'],
        'description': form_data['description'],
        'cost_dollar': float_or_int(form_data['price']),
        'owner_id': user_id,
        'amount': float_or_int(form_data['amount']),
        'logo': logo.filename if logo else 'no_logo_coin.png'
    }
    db.insert(table_name='currencies', values=new_currency)
    return render_template("add_currency_page.html")


@app.route("/add_currency")
@login_required
def add_currency_view():
    return render_template("add_currency_page.html")


@app.route("/logout")
@login_required
def logout():
    session['user'] = None
    return redirect(url_for('login_view', next=request.url))


@app.route("/profile", methods=['post', 'get'])
@login_required
def profile_view():
    user = get_user()
    user_currencies = db.filter(
        table_name='currencies JOIN users_currencies ON currencies.id = users_currencies.currency_id',
        args_to_select=('currency_id', 'name', 'count', 'logo'),
        user_id=user.get('id', None))
    if request.method == 'POST':
        form = request.form
        if form.get('donate', None):
            donated_balanced = float_or_int(form.get('donate'))
            current_balance = float_or_int(user['balance'])
            db.update(table_name='users', key_value=('balance', float_or_int(current_balance + donated_balanced)),
                      username=user['username'])
            user = db.filter(table_name='users', username=session.get('user', 'admin'))[0]
    return render_template("profile_page.html", **{
        'user': user,
        'user_currencies': user_currencies,
        'sell': 'true'
    })


@app.route('/favorites')
@login_required
def favorites_view():
    favorites = session.get(session.get('user', ''), [])
    currencies = db.select(table_name='currencies')
    favorite_currencies = []
    for currency in currencies:
        if currency['id'] in favorites:
            favorite_currencies.append(currency)
    return render_template('favorites_page.html', favorites=favorite_currencies)


@app.route('/add_to_favorite')
@login_required
def add_to_favorite_view():
    if not request.args:
        return redirect(url_for('login_view', next=request.url))
    cur_id = request.args.get('currency_id', None)
    username = session['user']
    operation = request.args['operation']
    session.modified = True
    if operation == 'add':
        if int(cur_id) not in session[username]:
            session[username].append(int(cur_id))
    elif operation == 'remove':
        if int(cur_id) in session[username]:
            session[username].remove(int(cur_id))
    return session[username]


@app.route('/edit_currency/<int:currency_id>', methods=['get', 'post'])
@login_required
def edit_currency_view(currency_id):
    user = get_user()
    currency = db.filter('currencies', id=currency_id)[0]
    if user.get('id') != currency.get('owner_id') and user.get('role') != 'admin':
        return redirect(url_for('main_view', next=request.url))
    if request.method == 'POST':
        form = request.form
        for k, v in form.items():
            db.update(table_name='currencies', key_value=(k, v), id=currency_id)
        currency = db.filter('currencies', id=currency_id)[0]
    return render_template('edit_currency_page.html', currency=currency)


@app.route('/buy_currency')
def buy_currency_view():
    if not request.args:
        return redirect(url_for('login_view', next=request.url))
    user = get_user()
    balance = float_or_int(request.args.get('balance', 0))
    payment = float_or_int(request.args.get('payment', 1))
    currency_id = float_or_int(request.args.get('currency_id'))
    currency = db.filter(table_name='currencies', id=currency_id)[0]
    currency_amount = float_or_int(currency['amount'])
    amount_to_get = payment / float_or_int(currency['cost_dollar'])
    if payment > balance:
        return "You can not pay more than you already have!"
    elif amount_to_get > currency_amount:
        return "Currency amount is not enough!"
    else:
        user_currency = db.filter(table_name='users_currencies',
                                  user_id=user['id'],
                                  currency_id=currency_id)
        if not user_currency:
            user_new_currency = {
                "user_id": user['id'],
                "currency_id": currency_id,
                "count": 0
            }
            db.insert(table_name='users_currencies',
                      values=user_new_currency)
        user_currency = db.filter(table_name='users_currencies',
                                  user_id=user['id'],
                                  currency_id=currency_id)[0]
        db.update(table_name='users_currencies',
                  key_value=('count', float_or_int(float(user_currency['count']) + amount_to_get)),
                  user_id=user['id'],
                  currency_id=currency_id)
        print(currency_amount - amount_to_get)
        db.update(table_name='currencies',
                  key_value=('amount', float_or_int(currency_amount - amount_to_get)),
                  id=currency_id)
        db.update(table_name='users',
                  key_value=('balance', float_or_int(balance - payment)),
                  id=user['id'])
        cur_owner = db.filter(table_name='users',
                              id=currency['owner_id'])[0]
        db.update(table_name='users',
                  key_value=('balance', float_or_int(float(cur_owner['balance']) + payment)),
                  id=cur_owner['id'])
        db.insert(table_name='transaction_history', values={
            'currency_id': currency_id,
            'seller_id': cur_owner['id'],
            'buyer_id': user['id'],
            'amount': amount_to_get,
            'payed_sum': payment
        })
        return "Success!"


@app.route('/sell_currency')
def sell_currency_view():
    if not request.args:
        return redirect(url_for('login_view', next=request.url))
    user = get_user()
    available_amount = float_or_int(request.args.get('available_amount', 0))
    amount_to_sell = float_or_int(request.args.get('amount_to_sell', 0.1))
    price_for_piece = float_or_int(request.args.get('price_for_piece', 1))
    currency_name = request.args.get('currency_name', '')
    logo = request.args.get('logo', 'no_logo_coin.png')
    old_id = float_or_int(request.args.get('old_id', None))
    if amount_to_sell > available_amount:
        return "You can not sell more than you already have!"
    else:
        old_currency = db.filter(table_name='currencies', id=old_id)[0]
        new_currency = {
            'name': currency_name,
            'description': old_currency['description'],
            'cost_dollar': price_for_piece,
            'owner_id': user['id'],
            'amount': amount_to_sell,
            'logo': logo
        }
        db.insert(table_name='currencies',
                  values=new_currency)
        db.update(table_name='users_currencies',
                  key_value=('count', float_or_int(available_amount - amount_to_sell)),
                  user_id=user['id'], currency_id=old_id)
        return 'Success!'


@app.route("/market", methods=['get', 'post'])
def main_view():
    session.modified = True
    user = get_user()
    favorite = session.get(session.get('user', ''), None)
    print('fav', favorite)
    context = {
        'currency_to_dollar': currency_to_dollar,
        'user': user,
        'favorite': favorite
    }
    if request.args.get('selected', None):
        selected = request.args.get('selected', None)
        prev = request.args.get('prev_selected', None)
        current_value = db.filter('currency_to_dollar', name=prev)[0] \
            .get('cost_to_dollar', 1)
        new_value_cur = db.filter('currency_to_dollar', name=selected)[0]
        new_value = new_value_cur.get('cost_to_dollar', 1)
        currency_mark = new_value_cur.get('mark', '$')
        return {'current_value': float_or_int(current_value),
                'new_value': float_or_int(new_value),
                'currency_mark': currency_mark}
    currencies = db.select(table_name="currencies")
    context['currencies'] = currencies
    return render_template('market_page.html', **context)


# to run main.py
if __name__ == "__main__":
    app.run(port=5000, host='localhost', debug=True)
