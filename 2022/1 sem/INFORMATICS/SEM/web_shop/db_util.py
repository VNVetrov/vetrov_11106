import json
from typing import List

import psycopg2

# import config
with open('config.json', 'r') as f:
    config = json.load(f)

DB_SETTINGS = {
    "dbname": config["dbname"],
    "user": config["user"],
    "password": config["password"],
    "host": config["host"],
    "port": config["port"]
}

LOOKUPS = {
    '__lt': '<',
    '__gt': '>',
    '__lte': '<=',
    '__gte': '>=',
    '__exact': '='
}


def get_lookup(cond: str):
    return LOOKUPS.get(cond[cond.rfind('__'):])


class Database:
    _FILTER_PATTERN = " WHERE %s"
    _SELECT_PATTERN = "SELECT %s FROM %s"
    _INSERT_PATTERN = "INSERT INTO %s %s VALUES %s"
    _UPDATE_PATTERN = "UPDATE %s SET %s"
    _DELETE_PATTERN = "DELETE FROM %s"

    def __init__(self):
        self.con = psycopg2.connect(**DB_SETTINGS)
        self.cur = self.con.cursor()

    @staticmethod
    def prepare_conditions(query, **conditions):
        filters: List[str] = []
        for k, v in conditions.items():
            op = '='
            if '__' in k:
                op = get_lookup(k)
                k = k[:k.rfind('__')]
            if isinstance(v, str):
                v = f"\'{v}\'"
            filters.append(f"{k}{op}{v}")
        where_part = " AND ".join(filters)
        query += Database._FILTER_PATTERN
        return where_part, query

    def execute(self, query, *params, get=False):
        final_query = query % params
        print(f"QUERY: {final_query}")
        if 'drop' not in final_query.lower():
            self.cur.execute(final_query)
        else:
            raise Exception(f"Somebody is trying to drop smth: {final_query}")
        if get:
            data = self.prepare_data(self.cur.fetchall())
            return data
        self.con.commit()

    def prepare_data(self, data):
        objects = []
        if len(data):
            column_names = [desc[0] for desc in self.cur.description]
            for row in data:
                objects += [{c_name: row[key] for key, c_name in enumerate(column_names)}]
        return objects

    def _select(self, table_name, args_to_select: tuple = "*", **conditions):
        column_names = ", ".join(args_to_select)
        query = Database._SELECT_PATTERN
        if conditions:
            where_part, query = self.prepare_conditions(query, **conditions)
            return self.execute(query, column_names, table_name, where_part, get=True)
        else:
            return self.execute(query, column_names, table_name, get=True)

    def select(self, table_name: str, args_to_select: tuple = "*"):
        return self._select(table_name=table_name,
                            args_to_select=args_to_select)

    def filter(self, table_name, args_to_select: tuple = "*", **conditions):
        return self._select(table_name=table_name,
                            args_to_select=args_to_select,
                            **conditions)

    def insert(self, table_name, values: dict):
        columns_part = tuple(values.keys())
        reformatted_columns_part = str(columns_part).replace('\'', '')
        values_part = tuple(values.values())
        query = Database._INSERT_PATTERN
        return self.execute(query, table_name, reformatted_columns_part, values_part, get=False)

    def update(self, table_name, key_value: tuple, **conditions):
        query = Database._UPDATE_PATTERN
        key = key_value[0]
        value = key_value[1]
        if isinstance(value, str):
            value = f"\'{value}\'"
        formatted_key_value = f"{key}={value}"
        where_part, query = self.prepare_conditions(query, **conditions)
        self.execute(query, table_name, formatted_key_value, where_part, get=False)

    def delete(self, table_name, **conditions):
        query = Database._DELETE_PATTERN
        where_part, query = self.prepare_conditions(query, **conditions)
        self.execute(query, table_name, where_part, get=False)
