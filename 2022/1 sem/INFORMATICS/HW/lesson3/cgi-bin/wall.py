#!/usr/bin/env python3

import cgi
from typing import Dict

from util import Util

form = cgi.FieldStorage()
action = form.getfirst("action")

login = form.getfirst("login")
password = form.getfirst("password")
title = form.getfirst("title")
text = form.getfirst("text")

util = Util()

online = util.get_data(util.ONLINE)
user = online[-1] if len(online) else None

error = False
message = ''
posts = ''


def get_posts():
    current_posts = ''
    post_pattern = '''
        <h2>{0}</h2>
        <p style="padding-bottom: 20px">{1}</p>
        '''
    current_user = user
    all_posts_for_current_user = "<h1>Посты:</h1>"
    posts_data: Dict = util.get_data(util.POSTS)
    if posts_data.get(current_user, None):
        for post_id in posts_data[current_user]:
            if post_id == "counter":
                continue
            post_title = posts_data[current_user][post_id]['Title']
            post_text = posts_data[current_user][post_id]['Text']
            all_posts_for_current_user += post_pattern.format(post_title, post_text)
    if all_posts_for_current_user != "<h1>Посты:</h1>":
        current_posts = all_posts_for_current_user
    return current_posts


def add_new_post(current_user, new_title, new_text):
    current_posts: Dict = util.get_data(util.POSTS)
    new_post = {
        "Title": new_title,
        "Text": new_text
    }
    username_or_none = current_posts.get(current_user, None)
    if username_or_none:  # If there are already some posts for user
        counter = int(current_posts[current_user]["counter"])
        current_posts[current_user][counter] = new_post
        current_posts[current_user]["counter"] = str(counter + 1)
        updated_posts = current_posts
    else:  # If there are no posts for user
        current_posts[current_user] = {
            "counter": "1",
            "0": new_post
        }
        updated_posts = current_posts
    util.set_data(util.POSTS, updated_posts)


if action == "login":
    if util.login(login, password):
        user = login
    else:
        error = True
        message = '<p>Такой пользователь не зарегистрирован</p>'
elif action == "register":
    util.register(login, password)
    message = '<p>Вы зарегистрированы и уже авторизованы</p>'
elif action == "logout":
    util.logout(user)
    message = "<p>Вы вышли из системы</p>"
elif action == "add_post":
    if not title or not text:
        message = "<p>Нельзя добавлять пост с пустым содержанием!</p>"
    else:
        add_new_post(user, title, text)
        message = "<p>Пост добавлен!</p>"
elif not action:
    if not util.is_online(user):
        action = "logout"

if action == "logout" or error:
    form = '''
        <h1>Авторизуйтесь</h1>
        <form method="post" action="wall.py">
            Логин: <input type="text" name="login">
            Пароль: <input type="password" name="password">
            <input type="hidden" name="action" value="login">
            <input type="submit">
        </form>
        <h1>Еще не зарегистрированы?</h1>
        <form method="post" action="wall.py">
            Логин: <input type="text" name="login">
            Пароль: <input type="password" name="password">
            <input type="hidden" name="action" value="register">
            <input type="submit">
        </form>
    '''
else:
    posts = get_posts()
    form = '''
        <form action="wall.py">
            <input type="hidden" name="action" value="logout">
            <input type="submit" value="Выйти">
        </form>
        <h1>Добавить пост</h1>
        <form method="post" action="wall.py">
            Название поста: <input type="text" name="title" style="margin-bottom: 10px">
            <br>
            Текст поста: <textarea name="text"></textarea>
            <input type="hidden" name="action" value="add_post">
            <br>
            <input type="submit" style="margin-top: 10px">
        </form>
    '''

pattern = '''
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>Wall</title>
</head>
<body>
    {form}
    
    {message}
    
    {posts}
</body>
</html>
'''

print('Content-type: text/html\n')
print(pattern.format(form=form, message=message, posts=posts))
