from typing import List

import psycopg2

LOOKUPS = {
    '__lt': '<',
    '__gt': '>',
    '__lte': '<=',
    '__gte': '>=',
    '__exact': '='
}


def get_lookup(cond: str):
    return LOOKUPS.get(cond[cond.rfind('__'):])


class Database:
    _SELECT_PATTERN = 'SELECT %s FROM %s'
    _INSERT_PATTERN = 'INSERT INTO %s VALUES %s'

    def __init__(self):
        self.con = psycopg2.connect(
            dbname="films",
            user="films",
            password="films",
            host="localhost",
            port=5432
        )
        self.cur = self.con.cursor()

    def execute(self, query, get=False):
        self.cur.execute(query)
        if get:
            data = self.prepare_data(self.cur.fetchall())
            if len(data) == 1:
                data = data[0]
            return data
        self.con.commit()

    def prepare_data(self, data):
        films = []
        if len(data):
            column_names = [desc[0] for desc in self.cur.description]
            for row in data:
                films += [{c_name: row[key] for key, c_name in enumerate(column_names)}]
        return films

    def _select(self, table_name, args_to_select: tuple = "*", **conditions):
        query = Database._SELECT_PATTERN % (", ".join(args_to_select), table_name)
        if conditions:
            filters: List[str] = []
            for k, v in conditions.items():
                if v or v == 0:
                    op = '='
                    if '__' in k:
                        op = get_lookup(k)
                        k = k[:k.rfind('__')]
                    if isinstance(v, str):
                        v = f"\'{v}\'"
                    filters.append(f"{k}{op}{v}")
            where_part = f" WHERE {' AND '.join(filters)}"
            query += where_part
        print(f"QUERY: {query}")
        return self.execute(query, get=True)

    def select(self, table_name: str, args_to_select: tuple = "*"):
        return self._select(table_name=table_name,
                            args_to_select=args_to_select)

    def filter(self, table_name, args_to_select: tuple = "*", **conditions):
        return self._select(table_name=table_name,
                            args_to_select=args_to_select,
                            **conditions)

    def insert(self, table_name, values: dict):
        values_part = tuple(values.values())
        query = Database._INSERT_PATTERN % (table_name, values_part)
        print(f"QUERY: {query}")
        return self.execute(query)
