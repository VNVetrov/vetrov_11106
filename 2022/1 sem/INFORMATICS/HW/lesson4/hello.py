import json
from flask import *

app = Flask(__name__)


def get_data():
    with open("films.json", 'r') as f:
        films = json.load(fp=f)
    return films


def set_data(content):
    with open('films.json', 'w') as f:
        json.dump(obj=content, fp=f)


FILMS = get_data()


@app.route("/")
def redirect_to_main():
    return redirect("/films")


def rating__gte_filter(films, rating: float):
    filtered_films = []
    for film in films:
        if film['rating'] >= rating:
            filtered_films.append(film)
    return filtered_films


def country__exact_filter(films, country):
    filtered_films = []
    for film in films:
        if film['country'] == country:
            filtered_films.append(film)
    return filtered_films


@app.route("/films")
def films_list():
    films = FILMS
    country = request.args.get("country")
    min_rating = request.args.get("rating")

    # filtering
    if country:
        films = country__exact_filter(films, country=country)
    if min_rating:
        films = rating__gte_filter(films, rating=float(min_rating))

    context = {
        'films': films,
        'title': "FILMS",
        'country': country
    }

    return render_template("films.html", **context)


@app.route("/film/<int:film_id>")
def get_film(film_id):
    for film in FILMS:
        if film['id'] == film_id:
            return render_template("film.html", title=film['name'], film=film)
    # error case
    return render_template("error.html", error="Такого фильма не существует в системе")


@app.get("/films/add_film")
def add_film_get():
    return render_template("add_film.html")


@app.post("/films/add_film")
def add_film_post():
    form_data = request.form.to_dict()
    film = {
        "id": len(FILMS),
        "name": form_data['name'],
        "rating": float(form_data['rating']),
        "country": form_data['country']
    }
    FILMS.append(film)
    set_data(FILMS)
    return render_template("add_film.html", status="Film added!")
