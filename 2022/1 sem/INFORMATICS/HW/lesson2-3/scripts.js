function is_not_null(el) {
    if (el === null || el.value === '') {
        el.style.border = border_error_style
        if (!is_alerted) {
            is_alerted = true
            return alert(`All field are required!`)
        }
    } else {
        el.style.border = border_non_error
    }
}
const name = document.getElementById('firstname');
const username = document.getElementById('username')
const birthdate = document.getElementById('birthdate')
const password = document.getElementById('password')
const conf_password = document.getElementById('conf_password')
const form = document.getElementById('form')
const border_error_style = "3px solid red"
const border_non_error = "none"
let now_date = new Date(),
    is_alerted = false
const current_date = now_date.getFullYear() + "-" + ("0" + (now_date.getMonth() + 1)).slice(-2) + "-" + ("0" + now_date.getDate()).slice(-2)
form.addEventListener('submit', (e) => {
    e.preventDefault()
    is_not_null(username)
    is_not_null(name)
    is_not_null(password)
    is_not_null(conf_password)
    is_not_null(birthdate)
    console.log(birthdate.value, current_date)
    if (username.value.length < 6) {
        username.style.border = border_error_style
        is_alerted = false
        return alert("Username must be longer than 5 characters!")
    } else if (birthdate.value > current_date) {
        birthdate.style.border = border_error_style
        is_alerted = false
        return alert("Your birthdate cannot be later than the current date!")
    } else if (password.value !== conf_password.value) {
        password.style.border = border_error_style
        conf_password.style.border = border_error_style
        is_alerted = false
        return alert("Passwords must the same!")
    } else {
        if (!is_alerted) {
            return alert("You have successfully registered!")
        }
    }
})


