import datetime

from flask import *

from db_util import Database

app = Flask(__name__)
app.secret_key = "111"
app.permanent_session_lifetime = datetime.timedelta(days=365)

db = Database()


@app.route("/")
def redirect_to_main_page():
    return redirect("/films")


@app.route("/add_cookie")
def add_cookie():
    resp = make_response("Add cookie")
    resp.set_cookie('light_mode', '0')
    return resp


@app.route("/visits")
def visits():
    visits_count = session['visits'] if 'visits' in session.keys() else 0
    session['visits'] = visits_count + 1
    return f"Количество визитов: {session['visits']}"


@app.route("/delete_visits")
def delete_visits():
    session.pop('visits')
    return "ok"


@app.route("/films")
def films_list():
    films = db.select(table_name='films')

    country = request.args.get("country")
    min_rating = request.args.get("rating")

    if country or min_rating:
        films = db.filter(table_name='films', country__exact=country, rating__gte=min_rating)

    return render_template("films.html", **{
        'films': films,
        'title': "FILMS",
        'light_mode': request.cookies.get('light_mode')
    })


@app.route("/films/<int:film_id>")
def get_film(film_id):
    film = db.filter(table_name='films', id=film_id)

    if film:
        return render_template("film.html", title=film['name'], film=film, light_mode=request.cookies.get('light_mode'))

    return render_template("error.html", **{
        'error': "Такого фильма не существует в системе",
        'light_mode': request.cookies.get('light_mode')
    })


@app.get("/films/add_film")
def add_film_get():
    return render_template("add_film.html", light_mode=request.cookies.get('light_mode'))


@app.post("/films/add_film")
def add_film_post():
    films = db.select('films')
    form_data = request.form.to_dict()
    film = {
        "id": len(films),
        "name": form_data['name'],
        "rating": float(form_data['rating']),
        "country": form_data['country']
    }
    db.insert(table_name='films', values=film)
    return render_template("add_film.html", status="Film added!", light_mode=request.cookies.get('light_mode'))


@app.route("/change_mode")
def change_theme():
    return render_template('change_mode.html', **{
        'light_mode': request.cookies.get('light_mode')
    })


@app.post("/change_mode")
def change_theme_post():
    res = make_response("Theme changed! <a href='/films'>MAIN</a>")
    lm = request.form.get('light_mode')
    current = request.cookies.get('light_mode')
    if lm != current:
        res.set_cookie('light_mode', str(lm))
    return res


if __name__ == '__main__':
    app.run(port=8000, debug=True)
