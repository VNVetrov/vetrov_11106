﻿using System;

namespace MyFirstProject
{
    class Program
    {
        static void Main(string[] args)
        {
            //Insert the coefficents
            var a = double.Parse(Console.ReadLine());
            var b = double.Parse(Console.ReadLine());
            var c = double.Parse(Console.ReadLine());
            //Calculating discrim.
            double D = b * b - 4 * a * c;
            //Determing disrim.
            if (D > 0)
            {
                //Solutions
                double x1 = ((-1) * b + Math.Sqrt(D)) / (2 * a);
                double x2 = ((-1) * b - Math.Sqrt(D)) / (2 * a);
                //Output
                Console.WriteLine("Два корня:");
                Console.WriteLine(x1);
                Console.WriteLine(x2);
            }
            else if (D == 0)

            {
                //Solution
                double x1 = (-1) * b / (2 * a);
                //Output
                Console.WriteLine("Один корень:");
                Console.WriteLine(x1);

            }
            else
            {
                //Output
                Console.WriteLine("There is no solutions");
            }
            
        }
    }
}
