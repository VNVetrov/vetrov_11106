﻿using System;

namespace Rec3
{
    class Program
    {
        static int counter = 0;
        static void Main()
        {
            int N = int.Parse(Console.ReadLine());
            var board = new bool[N, N];
            var row = 0;
            Task(board, N, row);
            Console.WriteLine(counter);
        }
        static bool IsNotAttackable(bool[,] board, int N, int row, int column)
        {
            for (int x= 0; x < N; x++)
            {
                for (int y = 0; y < N; y++)
                {
                    if (!(x == row && y==column))
                    {
                        int dx = Math.Abs(x - row);
                        int dy = Math.Abs(y - column);
                        if (dx==dy || dy == 0 || dx == 0)
                            if (board[x, y]) return false;
                    }
                }
            }
            return true;
        }
        static void Task(bool[,] board, int N, int row)
        {
            if (row == N)
            {
                counter++;
                for (int i = 0; i < N; i++)
                {
                    for (int j = 0; j < N; j++)
                    {
                        var x = board[i, j] ? 1 : 0;
                        Console.Write(x +" ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            else
            {
                for (int y = 0; y < N; y++)
                {
                    if (IsNotAttackable(board,N,row,y))
                    {
                        board[row, y] = true;
                        Task(board, N, row + 1);
                        board[row, y] = false;
                    }
                }
                
            }
        }
    }
}
