﻿using System;

namespace Rec1
{
    public class Program
    {
        public static int Pow(int number, int power)
        {
            int result = 1;
            int i = 0;
            while(i < power)
                if (i * 2 > power || i==0)
                {
                    result *= number;
                    i++;
                }
                else
                {
                    result *= result;
                    i *= 2;
                }
            return result;
        }
    }
}
