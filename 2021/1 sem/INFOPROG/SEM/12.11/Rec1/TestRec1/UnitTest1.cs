using NUnit.Framework;

namespace TestRec1
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {    
        }
        [TestCase(new int[] { 2, 17 }, 131072)]
        [TestCase(new int[] { 2, 10 }, 1024)]
        [TestCase(new int[] { 2, 11 }, 2048)]
        [TestCase(new int[] { 2, 4 }, 16)]
        [TestCase(new int[] { 2, 0 }, 1)]
        [TestCase(new int[] { 2, 1 }, 2)]
        [TestCase(new int[] { 2, 2 }, 4)]
        [Test]
        public static void RunTests(int[] input, int expectedOutput)
        {
            var actual = Rec1.Program.Pow(input[0],input[1]);
            Assert.AreEqual(actual, expectedOutput);
        }
    }
}