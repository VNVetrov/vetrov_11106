﻿using System;

namespace Ex11
{
    class Program
    {
        static void Main()
        {
            int hours = int.Parse(Console.ReadLine()) % 24;
            int minutes = int.Parse(Console.ReadLine()) % 60;
            int hoursAngle = 360 / 12 * hours;
            int minutesAngle = 360 / 12 * minutes;
            int answer = Math.Abs(hoursAngle - minutesAngle) % 360;
            Console.WriteLine(answer);

        }
    }
}
