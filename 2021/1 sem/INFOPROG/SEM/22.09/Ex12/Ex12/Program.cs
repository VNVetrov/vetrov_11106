﻿using System;

namespace Ex12
{
    class Program
    {
        static void Main()
        {
            string[] input = Console.ReadLine().Split(" ");
            int height = int.Parse(input[0]);
            int time = int.Parse(input[1]);
            int maxVelocity = int.Parse(input[2]);
            int silentVelocity = int.Parse(input[3]);
            var minTime = (double)(height - (time * silentVelocity)) / (maxVelocity - (silentVelocity));
            double maxTime;
            if (height < silentVelocity * time)
            {
                minTime = 0;
                maxTime = height / (double)(silentVelocity + 10e-6);
            }
            else
            {
                maxTime = time;
            }
            Console.WriteLine("{0} {1}",minTime,maxTime);

        }
    }
}
