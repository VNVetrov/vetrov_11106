﻿using System;
using System.Windows.Forms;
using System.Drawing;


namespace GravityBalls
{
	public class WorldModel
	{
		public double BallX;
		public double BallY;
		public double BallRadius;
		public double WorldWidth;
		public double WorldHeight;
		public double BallSpeedX;
		public double BallSpeedY;
		public double BallSpeedResistance;
		public double Gravity;
		public double PushSpeed;


		void BallMove(double dt)
        {
			BallSpeedY -= Gravity;
			BallX = BallX + BallSpeedX * dt;
			BallY = BallY + BallSpeedY * dt;
		}
		void BounceCheck()
        {
			if (BallX >= WorldWidth - BallRadius || BallX <= BallRadius)
			{
				BallSpeedX = -BallSpeedX;
				//Tried to change color while bounce
				//Brush BallColor = Brushes.Green;
				if (BallX >= WorldWidth - BallRadius) BallX = WorldWidth - BallRadius;
				else BallX = BallRadius;
			}
			else if (BallY >= WorldHeight - BallRadius || BallY <= BallRadius)
			{
				BallSpeedY = -BallSpeedY;
				//Tried to change color while bounce
				//Brush BallColor = Brushes.Blue;
				if (Math.Abs(BallSpeedY) < Math.Abs(Gravity)) Gravity = 0;
				else Gravity = -8;
				if (BallY >= WorldHeight - BallRadius) BallY = WorldHeight - BallRadius;
				else BallY = BallRadius;
			}
		}
		void BallResist()
        {
			if (BallSpeedX > 0) BallSpeedX -= BallSpeedResistance;
			else BallSpeedX += BallSpeedResistance;
			if (BallSpeedY > 0) BallSpeedY -= BallSpeedResistance; 
			else BallSpeedY += BallSpeedResistance;
		}
		void BallRepulse()
        {
			int cursorX = Cursor.Position.X;
			int cursorY = Cursor.Position.Y;
			var distance = Math.Sqrt((cursorX - BallX) * (cursorX - BallX) + (cursorY - BallY) * (cursorY - BallY));
			var speedDifference = PushSpeed / distance;
			if (cursorX > BallX) BallSpeedX -= speedDifference;
			else if (cursorX < BallX) BallSpeedX += speedDifference;
			if (cursorY > BallY) BallSpeedY -= speedDifference;
			else if (cursorY > BallY) BallSpeedY += speedDifference;
		}
		void BallTeleport()
        {
			if (Control.MouseButtons == MouseButtons.Left)
			{
				BallX = Cursor.Position.X;
				BallY = Cursor.Position.Y;
			}

		}

		public void SimulateTimeframe(double dt)
		{
			BallMove(dt);
			BounceCheck();
			BallResist();
			BallRepulse();
			BallTeleport();
		}

	}
}