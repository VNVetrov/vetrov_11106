﻿using System;
using System.Collections.Generic;

namespace Col3
{
    class Program
    {
        static void Main()
        {
            int maxSumm = 0;
            int nowSumm = 0;
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                int number = int.Parse(Console.ReadLine());
                nowSumm += number;
                maxSumm = Math.Max(maxSumm, nowSumm);
                if (nowSumm < 0) nowSumm = 0;
            }
            Console.WriteLine(maxSumm);
        }
    }
}
