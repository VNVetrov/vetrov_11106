﻿using System;

namespace Col2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            var arr = new int[n+1];
            string input;
            int l, r, plus;
            for (int i = 0; i < n+1; i++)
            {
                input = Console.ReadLine();
                l = int.Parse(input.Split()[0]);
                r = int.Parse(input.Split()[1]);
                plus = int.Parse(input.Split()[2]);
                if (l == r)
                {
                    arr[l - 1] += plus;
                    arr[l] -= plus;
                }
                else
                {
                    arr[l-1] += plus;
                    arr[r] -= plus;
                }
            }
            for (int i = 1; i < n+1; i++)
            {
                arr[i] = arr[i - 1] + arr[i];
                
            }
            for (int i = 0; i < n; i++)
            {
                Console.Write("{0} ",arr[i]);
            }
        }
    }
}
