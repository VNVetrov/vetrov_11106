﻿using System;
using System.Collections.Generic;

namespace Col1
{
    class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            var list = new List<int>();
            list.Add(0);
            list.Add(int.Parse(Console.ReadLine()));
            for (int i = 2; i <n+1; i++)
            {
                list.Add(list[i-1]+int.Parse(Console.ReadLine()));
            }
            int q = int.Parse(Console.ReadLine());
            var str = new string[q];
            for (int i = 0; i < q; i++)
            {
                str[i] = Console.ReadLine();
            }
            int l, r;
            for (int i = 0; i < q; i++)
            {
                l = int.Parse(str[i].Split()[0]);
                r = int.Parse(str[i].Split()[1]);
                Console.WriteLine(list[r]-list[l-1]);
            }

        }
    }
}
