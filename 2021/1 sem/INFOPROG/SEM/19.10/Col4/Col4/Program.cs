﻿using System;

namespace Col4
{
    class Program
    {
        static void Main()
        {
            int n = int.Parse(Console.ReadLine());
            int m = int.Parse(Console.ReadLine());
            int maxSumm = 0;
            var array = new int[n + m];
            for (int i = 0; i < n; i++)
            {
                var number = int.Parse(Console.ReadLine());
                array[i] += number;
                array[i + m] += -number;
                if (i != 0) array[i] += array[i - 1];
            }
            for(int i=m-1;i<n;i++)
                if (array[i] > maxSumm)
                    maxSumm = array[i];
            Console.WriteLine(maxSumm);
        }
    }
}
