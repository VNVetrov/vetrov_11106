﻿using System;

namespace Arr2
{
    class Program
    {
        static int Count(int[] array,int item)
        {
            int k = 0;
            for(int i = 0; i < array.Length; i++)
            {
                if (array[i] == item) k++;
            }
            return k;
        }
        static void Main(string[] args)
        {
            //Arr1 and Arr2

            var arrS1 = Console.ReadLine().Split();
            var arrS2 = Console.ReadLine().Split();
            int[] arr1 = new int[arrS1.Length];
            int[] arr2 = new int[arrS2.Length];
            Console.WriteLine("Arr1:");
            for(int i=0;i<arrS1.Length;i++)
            {
                arr1[i] = int.Parse(arrS1[i]);
                Console.Write("{0} ",arr1[i]);
            }
            Console.WriteLine("\nArr2:");
            for(int i=0;i<arrS2.Length;i++)
            {
                arr2[i] = int.Parse(arrS2[i]);
                Console.Write("{0} ",arr2[i]);
            }
            //Unique Unification
            Console.WriteLine("\nUnique Unification:");
            var uniqueUnification = new int[arr1.Length + arr2.Length];
            for (int i = 0; i < arr1.Length; i++)
            {
                if (Count(uniqueUnification, arr1[i]) == 0) uniqueUnification[i] = arr1[i];
            }
            bool flag = true;
            for (int i = 0; i < arr2.Length; i++)
            {
                for (int j = 0; j < uniqueUnification.Length; j++)
                {
                    flag = true;
                    if (arr2[i] == uniqueUnification[j])
                    {
                        flag = false;
                        break;
                    }

                }
                if (flag && Count(uniqueUnification,arr2[i])==0)
                {
                    uniqueUnification[arr1.Length + i] = arr2[i];
                }
            }
            Array.Sort(uniqueUnification);
            for (int i = 0; i < uniqueUnification.Length; i++)
            {
                if (uniqueUnification[i] != 0) Console.Write("{0} ", uniqueUnification[i]);
            }
            //Unification
            Console.WriteLine("\nUnification:");
            var unification = new int[arr1.Length + arr2.Length];
            for(int i = 0; i < arr1.Length+arr2.Length; i++)
            {
                if (i < arr1.Length) unification[i] = arr1[i];
                else unification[i] = arr2[i - arr1.Length];
            }
            Array.Sort(unification);
            for (int i = 0; i < unification.Length; i++)
            {
                Console.Write("{0} ", unification[i]);
            }
            //Intersection
            Console.WriteLine("\nIntersection");
            var intersection = new int[Math.Min(arr1.Length, arr2.Length)];
            flag = true;
            int k = 0;
            for (int i = 0; i < arr2.Length; i++)
            {
               
                for (int j = 0; j < arr1.Length; j++)
                {
                    flag = true;
                    if (arr2[i] == arr1[j] )
                    {
                        flag = false;
                        break;
                    }
            
                }
                if (!flag && Count(intersection, arr2[i]) < Math.Min(Count(arr2, arr2[i]), Count(arr1, arr2[i])))
                {
                    intersection[k] = arr2[i];
                }
                k++;
            }
            Array.Sort(intersection);
            for (int i = 0; i < intersection.Length; i++)
            {
                if (intersection[i] != 0) Console.Write("{0} ", intersection[i]);
            }
            //Diffrence Arr1-Arr2
            Console.WriteLine("\nDifference Arr1 and Arr2:");
            var difference1 = new int[arr1.Length];
            var cloneArr2 = new int[arr2.Length];
            for (int i = 0; i < arr2.Length; i++)
            {
                cloneArr2[i] = arr2[i];
            }
            for(int i = 0; i < arr1.Length; i++)
            {
                difference1[i] = arr1[i];
                int index = Array.IndexOf(cloneArr2, difference1[i]);
                if (index != -1)
                {
                    cloneArr2[index] = 0;
                    difference1[i] = 0;
                }
            }
            for (int i = 0; i < difference1.Length; i++)
            {
                if (difference1[i] != 0) Console.Write("{0} ",difference1[i]);
            }
            //Diffrence Arr2-Arr1
            Console.WriteLine("\nDifference Arr2 and Arr1:");
            var difference2 = new int[arr2.Length];
            var cloneArr1 = new int[arr1.Length];
            for (int i = 0; i < arr1.Length; i++)
            {
                cloneArr1[i] = arr1[i];
            }
            for (int i = 0; i < arr2.Length; i++)
            {
                difference2[i] = arr2[i];
                int index = Array.IndexOf(cloneArr1, difference2[i]);
                if (index != -1)
                {
                    cloneArr1[index] = 0;
                    difference2[i] = 0;
                }
            }
            for (int i = 0; i < difference2.Length; i++)
            {
                if (difference2[i] != 0) Console.Write("{0} ", difference2[i]);
            }

        }
    }
}
