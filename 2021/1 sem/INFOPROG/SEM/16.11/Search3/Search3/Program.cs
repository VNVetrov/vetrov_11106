﻿using System;
using System.Collections.Generic;

namespace Search3
{
    class Program
    {
        static void Main()
        {
            int answer, number, neededNumber;
            List<int> list;
            Input(out number, out list, out neededNumber);
            list.Sort();
            answer = Output(number, neededNumber, list);
            Console.WriteLine(answer);
        }

        private static int Output(int number, int neededNumber, List<int> list)
        {
            int answer;
            if (number > neededNumber)
            {
                var newList = new List<int>(neededNumber);
                for (int i = number - neededNumber; i < number; i++)
                    newList.Add(list[i]);
                answer = BinSearch(newList, neededNumber, 1, newList[neededNumber - 1]);
            }
            else
                answer = BinSearch(list, neededNumber, 1, list[number - 1]);
            return answer;
        }

        private static void Input(out int number, out List<int> list, out int neededNumber)
        {
            number = int.Parse(Console.ReadLine());
            list = new List<int>(number);
            neededNumber = int.Parse(Console.ReadLine());
            for (int i = 0; i < number; i++)
                list.Add(int.Parse(Console.ReadLine()));
        }

        static bool Checked(List<int> list, int length,int neededNumber)
        {
            int counter = 0;
            for(int i = list.Count-1;i>-1;i--)
            {
                counter += list[i] / length;
                if (counter >= neededNumber) return true;
            }
            return false;
        }

        static int BinSearch(List<int> list,int neededNumber,int left, int right) {
            if (right - left <= 1) return left;
            while (right - left > 1)
            {
                int middle = (left + right) / 2;
                if (Checked(list,middle,neededNumber))
                    return BinSearch(list,neededNumber, middle, right);
                else
                    return BinSearch(list,neededNumber, left, middle);
            }
            return -1;
        }
    }
}
