﻿using System.Collections.Generic;

namespace GameOfLife
{
    /* Реализуйте игру в жизнь на прямоугольном конечном поле.
     
     На каждом ходе клетка меняет свое состояние по таким правилам:
     1. Если у нее менее 2 живых соседей или более трех живых — она становится мертвой (false).
     2. Если ровно 3 живых соседа, то клетка становится живой (true)
     3. Если ровно 2 живых соседа, то клетка сохраняет своё состояние.

     У каждой неграничной клетки есть 8 соседей (в том числе по диагонали)

    Работу над игрой постройте итеративно в стиле TDD:
        1. Сначала напишите какой-нибудь простейший тест в соседнем файле GameTest.cs. Тест должен быть красным.
        То есть должен проверять ещё нереализованное требование.
        2. Только потом напшишите простейшую реализацию, которая делает тест зеленым. 
        Не старайтесь реализовать всю логику, просто сделайте тест зеленым как можно быстрее.
        3. Повторяйте процесс, пока ещё можете придумать новые красные тесты.

     На каждый шаг (тест и реализация) у вас должно уходить не более 5 минут.
     Если вы не успели поднять тест за 5 минут — удалите этот тест и придумайте тест попроще.
     Засекайте время таймером на телефоне.

     После каждого шага (тест или реализация) меняйте активного человека за клавиатурой.

     Начните с простейших тестов. 

     Проект настроен так, что при каждой сборке запускаются все тесты и отчет выводится на консоль
    */
    public class Game
    {
        public static bool[,] NextStep(bool[,] field)
        {
            int counter;
            var xLength = field.GetLength(0);
            var yLength = field.GetLength(1);
            var newField = new bool[xLength, yLength];

            for (var x = 0; x < xLength; x++)
            {
                for (var y = 0; y < yLength; y++)
                {
                    counter = UpdateField(x, y, field);
                    if (counter < 2 || counter > 3) newField[x, y] = false;
                    else if (counter == 3) newField[x, y] = true;
                    else newField[x, y] = field[x, y];
                }
            }
            return newField;
        }

        private static int UpdateField(int x, int y, bool[,] field)
        {
            var xLength = field.GetLength(0);
            var yLength = field.GetLength(1);
            var aroundElemnts = new List<bool>();

            for (var dx = -1; dx < 2; dx++)
            {
                for (var dy = -1; dy < 2; dy++)
                {
                    if (IsInside(x + dx, y + dy, xLength, yLength))
                        aroundElemnts.Add(field[x + dx, y + dy]);
                }
            }
            return SetSharpsAround(aroundElemnts);
        }

        private static bool IsInside(int x, int y, int xLength, int yLength)
        {
            return (x + 1 > 0 && y + 1 > 0) && (x - xLength < 0 && y - yLength < 0);
        }
        
        private static int SetSharpsAround(List<bool> aroundElements)
        {
            int counter = 0;
            foreach (var item in aroundElements)
                if (item == true) counter++;
            return counter;
        }
    }
}
