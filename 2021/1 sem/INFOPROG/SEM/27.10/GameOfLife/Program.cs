﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        public static void Paint(bool[,] field)
        {
            Console.SetCursorPosition(0, 0);
            for (int y = 0; y < field.GetLength(1); y++)
            {
                for (int x = 0; x < field.GetLength(0); x++)
                {
                    var symbol = field[x, y] ? '#' : ' ';
                    Console.Write(symbol);
                }
                Console.WriteLine();
            }
        }

        static void Main(string[] args)
        {
            Random gen = new Random();
            var field = new bool[80, 20];
            for (int i = 20; i < field.GetLength(0)-20; i++)
            {
                for (int j = 3; j < field.GetLength(1)-3; j++)
                {
                    field[i, j] = gen.Next(100) <= 40 ? true : false;
                }
            }
            
            while (true)
            {
                Paint(field);
                Thread.Sleep(50);
                field = Game.NextStep(field);
            }
        }
    }
}
