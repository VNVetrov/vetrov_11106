﻿using System;

namespace Loops5
{
    class Program
    {
        static void Main()
        {
            string inputLine = Console.ReadLine();
            int counter = 0,max=0;
            for (int i = 0; i < inputLine.Length; i++)
            {
                if (inputLine[i] == '(') counter++;
                else counter--;
                if (counter >= max) max = counter;
                if (counter < 0) break;
            }
            if (counter == 0) Console.WriteLine("True, maximum depth: {0}",max);
            else Console.WriteLine("False");

        }
    }
}
