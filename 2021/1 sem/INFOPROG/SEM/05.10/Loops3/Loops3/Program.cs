﻿using System;

namespace Loops3
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int summDigits = 0, targetDigits=0,numDigits=1;
            int degreeTen = 1;
            while (summDigits<n)
            {
                targetDigits++;
                summDigits += 9 * degreeTen * numDigits;
                if (summDigits >= n)
                {
                    summDigits-= 9 * degreeTen * numDigits;
                    break;
                }
                numDigits++;
                degreeTen *= 10;
            }
            int index = n - summDigits;
            var numIndex = Math.Ceiling( (double)index / targetDigits);
            var indexDigits = index - (numIndex - 1) * targetDigits;
            string targetNumber = (degreeTen + (numIndex - 1)).ToString();
            Console.WriteLine(targetNumber[(int)indexDigits-1]);
        }
    }
}
