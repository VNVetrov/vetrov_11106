﻿using System;

namespace Cond6
{
    class Program
    { 
        static void FindLastDot(double x0,double y0,double x1,double y1, double x2, double y2)
        {
            var x3 = x1 + x2 - x0;
            var y3 = y1 + y2 - y0;
            Console.WriteLine("({0} {1}) is the fourth dot.", x3, y3);
        }
    
        static void Main()
        {
            string[] data = Console.ReadLine().Split(" ");
            double x0 = double.Parse(data[0]);
            double y0 = double.Parse(data[1]);
            double x1 = double.Parse(data[2]);
            double y1 = double.Parse(data[3]);
            double x2 = double.Parse(data[4]);
            double y2 = double.Parse(data[5]);
            //AB(x1-x0,y1-y0) BC(x2-x1,y2-y1) AC(x2-x0,y2-y0)
            var firstLenght = Math.Sqrt((x1 - x0) * (x1 - x0) + (y1 - y0) * (y1 - y0)); // vect 01 -1
            var secondLenght = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2)); //vect 12 -2
            var thirdLenght = Math.Sqrt((x0 - x2) * (x0 - x2) + (y0 - y2) * (y0 - y2));//vect 02 - 3
            //IsSquare 
            bool isSquare1 = ((x1 - x0) * (x1 - x2) + (y1 - y0) * (y1 - y2) == 0 && firstLenght == secondLenght); // 1 & 2
            bool isSquare2 = ((x2 - x0) * (x2 - x1) + (y2 - y0) * (y2 - y1) == 0 && secondLenght == thirdLenght); // 2 & 3
            bool isSquare3 = ((x1 - x0) * (x2 - x0) + (y1 - y0) * (y2 - y0) == 0 && firstLenght == thirdLenght); // 1 & 3
            //Result
            if (isSquare1) FindLastDot(x1, y1, x0, y0, x2, y2);
            if (isSquare2) FindLastDot(x2, y2, x1, y1, x0, y0);
            if (isSquare3) FindLastDot(x0, y0, x1, y1, x2, y2);
            else Console.WriteLine("It's not square");
        }
    }
}
