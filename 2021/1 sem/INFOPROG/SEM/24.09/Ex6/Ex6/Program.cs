﻿using System;

namespace Ex6
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Insert the coordinates of the point: (x y)");
            string[] dotCoord = Console.ReadLine().Replace(".",",").Split(" ");
            double x = double.Parse(dotCoord[0]);
            double y = double.Parse(dotCoord[1]);

            Console.WriteLine("Insert the coordinates of the points of the straight line: (x1 y1 x2 y2)");
            string[] lineCoord = Console.ReadLine().Replace(".", ",").Split(" ");
            double x0 = double.Parse(lineCoord[0]);
            double y0 = double.Parse(lineCoord[1]);
            double x1 = double.Parse(lineCoord[2]);
            double y1 = double.Parse(lineCoord[3]);

            var coeffA = y0 - y1;
            var coeffB = x1 - x0;
            var coeffC = x0 * y1 - x1 * y0;
            var distance = Math.Abs(coeffA * x + coeffB * y + coeffC) / Math.Sqrt(coeffA*coeffA+coeffB*coeffB);

            Console.WriteLine("The distance is: {0}",distance);



        }
    }
}
