﻿using System;

namespace Ex5
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Insert the [a,b]: (a b)");
            string[] data = Console.ReadLine().Split(" ");
            int a = int.Parse(data[0]);
            int b = int.Parse(data[1]);
            int countLeapA = (a-1) / 4 - (a-1) / 100 + (a-1) / 400;
            int countLeapB = b / 4 - b / 100 + b / 400;
            int answer = countLeapB - countLeapA;
            Console.WriteLine("The answer is: {0}",answer);
        }
    }
}
