﻿using System;

namespace Ex7
{
    class Program
    {
        static void Main()
        {
            var rand = new Random();

            Console.WriteLine("Insert the coefficents of line: (A B C)");
            string[] coordinates = Console.ReadLine().Replace(".",",").Split(" ");

            double coeffA = double.Parse(coordinates[0]);
            double coeffB = double.Parse(coordinates[1]);
            double coeffC = double.Parse(coordinates[2]);

            if (coeffB == 0 || coeffA == 0)
            {
                Console.WriteLine("Try to put correct coefficents.");
            }
            else
            {
                int randNum = rand.Next(2, 5);
                Console.WriteLine("Parallel vector might have (" + randNum * coeffB + "; " + randNum * -coeffA + ") coordinates.");
                Console.WriteLine("Perpendicular vector might have (" + coeffA*randNum + "; " + coeffB * randNum+ ") coordinates.");
            }
        }
    }
}
