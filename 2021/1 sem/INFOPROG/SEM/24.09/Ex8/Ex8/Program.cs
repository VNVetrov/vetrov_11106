﻿using System;

namespace Ex8
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine("Insert the coefficents of the line y = kx + b: (k b)");
            string[] lineData = Console.ReadLine().Replace(".",",").Split(" ");
            double kCoeff = double.Parse(lineData[0]);
            double bCoeff = double.Parse(lineData[1]);

            Console.WriteLine("Insert the coordinates of dot A: (x y)");
            string[] dotData = Console.ReadLine().Replace(".", ",").Split(" ");
            double x0 = double.Parse(dotData[0]);
            double y0 = double.Parse(dotData[1]);

            /* P: y=-1/k * x + b1
             * b1 = y0 + x/k */
            if (kCoeff == 0)
            {
                Console.WriteLine("The desired point has coordinates: {0} {1}", x0, bCoeff);
            }
            else
            {
                var xOverlap = (y0 + x0 / kCoeff - bCoeff) / (kCoeff + 1 / kCoeff);
                var yOverlap = kCoeff * xOverlap + bCoeff;

                Console.WriteLine("The desired point has coordinates: {0} {1}", xOverlap, yOverlap);
            }


        }
    }
}
