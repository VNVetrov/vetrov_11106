﻿using System;
using System.Collections.Generic;

namespace CW3//Вариант 1
{
    //Task 1
    public class Vector
    {
        public int X;
        public int Y;
    }

    public class Segment
    {
        public Vector firstVector;
        public Vector secondVector;

        public Segment(int x0, int y0, int x1, int y1)
        {
            firstVector = new Vector() { X = x0, Y = y0 };
            secondVector = new Vector() { X = x1, Y = y1 };
        }
        public Segment(Vector first, Vector second)
        {
            firstVector = first;
            secondVector = second;
        }

        public double GetLength()
        {
            double lenX = secondVector.X - firstVector.X;
            double lenY = secondVector.Y - firstVector.Y;
            return Math.Sqrt(lenX * lenX + lenY * lenY);
        }

        public double GetSquareArea()
        {
            var oXLen = Math.Abs(secondVector.X - firstVector.X);
            var oYLen = Math.Abs(secondVector.Y - firstVector.Y);
            return oXLen * oYLen;
        }

        public bool IsIntersected(Segment segment)
        {
            var x0 = this.firstVector.X;
            var y0 = this.firstVector.Y;
            var x1 = this.secondVector.X;
            var y1 = this.secondVector.Y;
            var x2 = segment.firstVector.X;
            var y2 = segment.firstVector.Y;
            var x3 = segment.secondVector.X;
            var y3 = segment.secondVector.Y;

            var k1 = (double)(y1 - y0) / (x1 - x0);
            var b1 = -x0 * k1 + y0;

            var k2 = (double)(y3 - y2) / (x3 - x2);
            var b2 = -x2 * k2 + y2;

            var x = (double)(b2 - b1) / (k1 - k2);

            return x >= Math.Min(x0, x1) && x <= Math.Max(x0, x1) && x >= Math.Min(x2, x3) && x <= Math.Max(x2, x3);
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as Segment);
        }

        public bool Equals(Segment segment)
        {
            return firstVector.X == segment.firstVector.X 
                && secondVector.X == segment.secondVector.X 
                && segment.firstVector.Y == firstVector.Y 
                && segment.secondVector.Y == secondVector.Y;
        }

        public override string ToString()
        {
            return $"First dot: X({firstVector.X}) Y({firstVector.Y})\nSecond dot: X({secondVector.X}) Y({secondVector.Y})";
        }
    }

    class Program
    {
        static double GetCommonLength(List<Segment> segments)
        {
            double commonLength = 0;
            foreach (Segment segment  in segments)
                commonLength += segment.GetLength();
            return commonLength;
        }
        static bool IsAnyTwoIntersected(List<Segment> segments)
        {
            for (int i = 0; i < segments.Count-1; i++)
            {
                for (int j = i+1; j < segments.Count; j++)
                {
                    if (segments[i].IsIntersected(segments[j])) return true;
                }
            }
            return false;
        }
        static void Main()
        {
            var seg1 = new Segment(2, 2, 5, 5);
            var seg2 = new Segment(new Vector() { X = 2, Y = 3 }, new Vector() { X = 5, Y = 1 });
            var array = new List<Segment>() { seg1, seg2 };
            Console.WriteLine(GetCommonLength(array));

            Console.WriteLine();
            Console.WriteLine(seg1);
            Console.WriteLine(seg2);
            Console.WriteLine("Is intersected:{0}",IsAnyTwoIntersected(array));


            Console.WriteLine();
            CW3.Main.Program();
        }
    }



    //Task 2
    public abstract class Sport
    {
        public abstract void LoveSport();
        
        public void MakeATournament()
        {
            Console.WriteLine("I made a new tournament!");
        }

    }

    public class Football : Sport
    {
        public override void LoveSport()
        {
            Console.WriteLine("I love football!");
        }

        public void KickBall()
        {
            Console.WriteLine("Kicking ball..");
        }
    }

    public class Basketball : Sport
    {
        public override void LoveSport()
        {
            Console.WriteLine("I love basketball!");
        }

        public void ThrowBall()
        {
            Console.WriteLine("Throwing ball...");
        }
    }

    public class Volleyball
    {
        public virtual void ChooseBall()
        {
            Console.WriteLine("Picking random ball...");
        }

        public void PlayBeachVolleyball()
        {
            Console.WriteLine("We play valleyball at beach!");
        }
    }

    public class VolleyballPlayer : Volleyball
    {
        public override void ChooseBall()
        {
            Console.WriteLine("Picking favourite ball of player...");
        }

        public void WearFavouriteBoots()
        {
            Console.WriteLine("I choose my favourite boots to play volleyball!");
        }
    }

    public class Main
    {
        public static void Program()
        {
            //Upcast
            Volleyball volleyball = new VolleyballPlayer();
            //Late binding
            volleyball.ChooseBall();
        }
    }
}
