﻿using System;
using System.Text;

namespace CW2
{
    class Program
    {
        static void Main()
        {
            //1 variant
            Task1();
            /*Tests for Task1:[input - output]
            1st test - Emptry string
            [string.Empty - string.Empty]
            2nd test - Only one "true" string
            ["true" - "true"]
            3rd test - Two "true" strings
            ["truetrue" - "truefalse"]
            4th test - Something after 2nd "true"
            ["trueSomethingtrueSomething" - "trueSomethingfalseSomething"]
            5th test - A lot of "true" strings
            ["truetruetruetruetruetruetrue" - "truefalsetruefalsetruefalsetrue"]
            */
            Task2();
            Task3();
        }
        public static void Task1()
        {
            var stringBuilder = new StringBuilder();
            var fullString = InputTask1();
            GetFinalString(stringBuilder, fullString);
            var finalString = stringBuilder.ToString();
            OutputTask1(finalString);
        }

        private static void OutputTask1(string finalString)
        {
            Console.WriteLine("Output is: {0}", finalString);
        }

        private static void GetFinalString(StringBuilder stringBuilder, string fullString)
        {
            int counter = 0;
            for (int i = 0; i < fullString.Length; i++)
            {
                if (i <= fullString.Length - 4 && fullString.Substring(i, 4) == "true")
                {
                    counter++;
                    if (counter > 0 && counter % 2 == 0)
                    {
                        stringBuilder.Append("false");
                        i += 3;
                    }
                    else
                    {
                        stringBuilder.Append(fullString[i]);
                    }

                }
                else
                {
                    stringBuilder.Append(fullString[i]);
                }
            }
        }

        private static string InputTask1()
        {
            Console.WriteLine("Input string:");
            var fullString = Console.ReadLine();
            return fullString;
        }
        public static void Task2()
        {
            Console.WriteLine("Input a number k:");
            int k = int.Parse(Console.ReadLine());
            RecursionOutput(k,2);
        }
        public static int RecursionOutput(int k, int multiplier)
        {
            if (multiplier < 10)
            {
                Console.WriteLine("{0} x {1} = {2}", multiplier, k, k * multiplier);
                return RecursionOutput(k, multiplier + 1);
            }
            return -1;

        }
        public static void Task3()
        {
            int x, y;
            InputTask3(out x, out y);
            int right = GetRightBorder(x);
            OutputTask3(x, y, right);
        }

        private static void InputTask3(out int x, out int y)
        {
            Console.WriteLine("Input X:");
            x = int.Parse(Console.ReadLine());
            Console.WriteLine("Input Y:");
            y = int.Parse(Console.ReadLine());
        }

        private static void OutputTask3(int x, int y, int right)
        {
            Console.WriteLine("Outpit is: {0}", SummAndMultiply(right, x, y));
        }

        private static int GetRightBorder(int x)
        {
            int left = 1, right = x;
            while (right - left > 1)
            {
                int middle = (left + right) / 2;
                if (IsSumAble(middle, x))
                    right = middle;
                else
                    left = middle;
            }

            return right;
        }

        public static bool IsSumAble(int number, int x)
        {
            int summ = 0;
            for (int i = number; i > 0; i--)
            {
                summ += i;
                if (summ >= x) return true;
            }
            return false;
        }
        public static int SummAndMultiply(int number, int x, int y)
        {
            int summ = 0;
            int multiplier = 1;
            for (int i = number; i > 0; i--)
            {
                summ += i;
                multiplier *= i;
                if (summ == x && multiplier == y) return number - i + 1;
            }
            return -1;
        }
    }
}
