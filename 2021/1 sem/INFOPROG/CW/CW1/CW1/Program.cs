﻿using System;

namespace CW1
{
    class Program
    {
        static void Main()
        {
            //Вариант 1
            //Task1();
            //Task2();
            Task3();

        }
        //Task1
        static bool Cond1(int input)
        {
            var str = input.ToString();
            for (int i = 0; i < str.Length; i++)
            {
                int digit = str[i] - '0';
                if (digit % 2 == 0)
                    return false;
            }
            return true;
        }
        static void Task1()
        {
            int k = 0;
            int input;
            int n = int.Parse(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                input = int.Parse(Console.ReadLine());
                //Число делится на 3 и состоит только из нечетных цифр
                if (input % 3 == 0 && Cond1(input)) k++;
            }
            Console.WriteLine(k);
        }
        //Task2
        static int Pow(int number, int power)
        {
            int result = number;
            for (int i = 1; i < power; i++)
                result *= number;
            return result;
        }
        static int Fact(int number)
        {
            int i = 1;
            int result=1;
            while (i<=number)
            {
                result *= i;
                i++;
            }
            return result;
        }
        static bool Cond2(int input)
        {
            var str = input.ToString();
            for (int i = 0; i < str.Length; i++)
            {
                int digit = str[i] - '0';
                if (digit % 2 == 0) return true;
            }
            return false;
        }
        static int SumOfDigits(int number)
        {
            int summ = 0;
            var str = number.ToString();
            for (int i = 0; i < str.Length; i++)
                summ += str[i] - '0';
            return summ;   
        }
        static void Task2()
        {
            int l = 0;
            int input;
            int n = int.Parse(Console.ReadLine());
            var array = new int[n];
            for (int i = 0; i < n; i++)
            {
                input = int.Parse(Console.ReadLine());
                array[i] = input;
            }
            for (int i = 0; i < array.Length; i++)
                l = FindAmount(l, array, i);
            Console.WriteLine(SumOfDigits(l));
        }

        private static int FindAmount(int l, int[] array, int i)
        {
            int b = (Pow(2, i) + Fact(i));
            int c = array[i];
            int disc = b * b - 4 * Fact(c);
            if (disc > 0)
            {
                var firstSol = (-b + Math.Sqrt(disc)) / 2;
                var secondSol = (-b - Math.Sqrt(disc)) / 2;
                if (Cond2((int)secondSol)) l++;
                if (Cond2((int)firstSol)) l++;
            }
            else if (disc == 0)
            {
                var solution = (-b / 2);
            }
            return l;
        }
        //Task3
        static void Task3()
        {
            int m = int.Parse(Console.ReadLine());
            int n = int.Parse(Console.ReadLine());
            int m1 = 0, n1 = 0;
            bool flag = false;
            var arrayM = new int[m];
            var arrayN = new int[n];
            var array = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < m; i++)
            {
                arrayM[i] = int.Parse(array[i]);
            }
            array = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            for (int i = 0; i < n; i++)
            {
                arrayN[i] = int.Parse(array[i]);
            }
            for (int i = 2; i < Math.Max(m, n); i++)
            {
                if (m % i == 0 && n % i == 0)
                {
                    flag = true;
                    m1 = m / i;
                    n1 = n / i;
                    break;
                }
            }
            MultiplyMatrix(m, n, m1, n1, flag, arrayM, arrayN);
        }

        private static void MultiplyMatrix(int m, int n, int m1, int n1, bool flag, int[] arrayM, int[] arrayN)
        {
            if (flag)
            {
                Console.WriteLine("Yes!");
                Console.WriteLine("{0}x{1} {2}x{3}", m/m1, m1, n/n1, n1);
                for (int i = 0; i < arrayM.Length; i += Math.Min(m1, n1))
                {
                    int number = 0;
                    for (int j = 0; j < arrayN.Length; j += Math.Max(m1, n1))
                    {
                        number += arrayM[i] * arrayN[i];

                    }
                    Console.Write("{0} ",number);
                    

                }
            }
            else
            {
                Console.WriteLine("No!");
            }
        }
    }
}
